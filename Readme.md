# MetaGer Keys

Local development can be done via docker compose. The app is configured to be run in a subdirectory of MetaGer. Before starting this app you should start up a local MetaGer instance.

The server can then be reached under:  
http://localhost:8080/keys

If you use this app without a MetaGer deployment it can be reached under
http://localhsot:8085

## Translation

[![Strings reviewed status](https://translate.metager.de/190c4e95-7bfd-4149-81a2-bf49e74df6ec/percentage_reviewed_badge.svg)
![Translations](https://translate.metager.de/190c4e95-7bfd-4149-81a2-bf49e74df6ec/translations_badge.svg)
![Reviewed](https://translate.metager.de/190c4e95-7bfd-4149-81a2-bf49e74df6ec/reviewed_badge.svg)
![Conflicts](https://translate.metager.de/190c4e95-7bfd-4149-81a2-bf49e74df6ec/conflicts_badge.svg)](https://translate.metager.de/app/projects/190c4e95-7bfd-4149-81a2-bf49e74df6ec)

## Configuration

Configuration files for this app are stored in the config directory. You can find a `default.json` there.

To overwrite configuration locally you can create a new file `development.json` which overwrites properties from the default values.
