# API Documentation

Keymanager supports a JSON API to manage and query keys.

## Authentication

Most API methods are only available authenticated. You need to supply an Authorization header with those requests.

```
Authorization: Bearer <API-Key>
```

## `POST /api/json/key/create`

Creates a key and charges it by the specified amount. Creates a order to do so. A note can be attached to the order.  
`manual` is currently the only valid payment processor for API use.  
Can take either amount or price. If both supplied only amount is taken into account.

### Parameters

```json
{
    "amount": <AMOUNT_TO_CHARGE>,
    "price": <PRICE_TO_CHARGE>,
}
```

### Example Response

Successfull discharge will have a response code of `201`  
If the key cannot be charged because it is already charged with too many orders and the response code will be `403`

```json
{
    "key": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "payment_reference": 123456,
    "charged": <AMOUNT_CHARGED>
}
```

## `GET /api/json/key/:key`

Retrieves detailed information for a given key.

### Example Response

```json
{
    "key": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "charge": 100,
    "expiration": "2024-03-01 00:00:00",
    "charge_orders": [
        {
            "amount": 50,
            "order_id": "111111111111",
            "expiration": "2024-03-01T00:00:00.000Z"
        },
        ...
    ]
}
```

## `POST /api/json/key/:key/discharge`

Reduces the charge of a key by specified amount.  
Can take either amount or price. If both supplied only amount is taken into account.

### Parameters

```json
{
    "amount": <AMOUNT_TO_DISCHARGE>,
    "price": <PRICE_TO_CHARGE>
}
```

### Example Response

Successfull discharge will have a response code of `201`

```json
{
    "key": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "charge": 100,
    "discharged": <AMOUNT_DISCHARGED>
}
```

## `POST /api/json/key/:key/charge`

Charges a key by specified amount. Creates a Order to do so. A note can be attached to the order.  
`manual` is currently the only valid payment processor for API use.  
Can take either amount or price. If both supplied only amount is taken into account.

### Parameters

```json
{
    "amount": <AMOUNT_TO_CHARGE>,
    "price": <PRICE_TO_CHARGE>,
}
```

### Example Response

Successfull discharge will have a response code of `201`  
If the key cannot be charged because it is already charged with too many Orders response code will be `423`

```json
{
    "key": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "payment_reference": 123456,
    "charged": <AMOUNT_CHARGED>
}
```

## `GET /api/json/token/pubkey`

Retrieves the pubkey the server is using currently for token signatures.  
This method does not require authentication

### Example Response

```json
{
  "date": "2023-03",
  "standard": "pcks8",
  "format": "pem",
  "pubkey_pem": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1knnta8kCSClaIPECAGZ\npD75MIRVD20Ucc/SQP3BFHVCoBhwLt77V3ORpwYph8Wzk9QNYjwXwme8Dd8CCdu1\noLKXbneUn3gR1f/yu2ghih64Qs7DRbIVCILDUmO3PCCePB811Dz5cBABjbUg64p3\nOJbJDtbWxcZYYd5GH3VOo0yhk7RKSZxGNzCnwtzRvhKzdl0hI5F8POA6rkql4WbA\nghXyspdSC3e0s6AN9plTGxzysW0Du/a3ly2WA3ycpQO9HWyxepl8AblYfUTRm5lb\ngX9q6JYbGQvkZCd1ejEmhpIZfiwXZsBo1dygCgjlBfIXfLgfAfQATLYOuubdAWUB\nfQIDAQAB\n-----END PUBLIC KEY-----"
}
```

## `POST /api/json/token/sign`

Signs submitted blinded tokens with the servers private key. The submitted key needs to hold a charge for each token or the request will fail.
The Key will be discharged by this action.  
You cannot submit more than 10 tokens to be signed.
This method does not require authentication

### Parameters

```json
{
    "key": <KEY_TO_BE_DISCHARGED>,
    "date": <DATE_AS_SUPPLIED_IN_TOKEN_PUBKEY>,
    "blinded_tokens": [
        "<BLINDED_TOKEN_BIGINT_AS_STRING>",
        ...
    ]
}
```

### Example Response

If all blinded tokens were signed successfully response code will be `201`.  
If the key isn't charged enough for the amount of tokens or if the date supplied is not valid response code will be `422`

```json
{
    "key": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "discharged": <AMOUNT_THAT_WAS_DISCHARGED>,
    "charge": <NEW_CHARGE_OF KEY>,
    "date": <DATE_AS_SUPPLIED_IN_TOKEN_PUBKEY>,
    "signed_tokens": {
        "<BLINDED_TOKEN_BIGINT_AS_STRING>": "<SIGNATURE_BIGINT_AS_STRING>",
        ...
    }
}
```

## `POST /api/json/token/check`

Checks supplied tokens for validity
You cannot submit more than 10 tokens to be used.

### Parameters

```json
{
    "tokens": [
        {
            "token": <TOKEN>,
            "signature": <SIGNATURE>,
            "date": <DATE_AS_SUPPLIED_IN_TOKEN_PUBKEY>,
        }
        ...
    ]
}
```

### Example Response

If all tokens were used successfully response code will be `201`.  
If any validation errors (signature, token format, etc.) occured response code will be `422`

```json
{
    TODO
}
```

## `POST /api/json/token/use`

Uses supplied tokens to be consumed for MetaGer search. All supplied tokens will be invalid after this action.
You cannot submit more than 10 tokens to be used.

### Parameters

```json
{
    "tokens": [
        {
            "token": <TOKEN>,
            "signature": <SIGNATURE>,
            "date": <DATE_AS_SUPPLIED_IN_TOKEN_PUBKEY>,
        }
        ...
    ]
}
```

### Example Response

If all tokens were used successfully response code will be `201`.  
If any validation errors (signature, token format, etc.) occured response code will be `422`

```json
{
    TODO
}
```
