const Knex = require("knex");
const dayjs = require("dayjs");
const config = require("config");

/**
 * @typedef {Object} ReceiptsDB
 * @property {number} id
 * @property {string} receipt
 * @property {string} invoice_id
 * @property {number} payment_id
 * @property {string} created_at
 */
/** @returns {Knex.QueryBuilder<ReceiptsDB, {}>} */
const Receipts = () => __database_client("receipts");

class Receipt {
  /**
   *
   * @param {Object} data
   * @param {number} data.id
   * @param {string} data.receipt
   * @param {string} invoice_id
   * @param {number} data.payment_id
   * @param {string} data.created_at
   */
  constructor(data) {
    this.id = data.id;
    this.public_id = "R" + (data.id + config.get("price.number_range.invoices"));
    if (data.id < 0) {
      this.public_id = "TEST_INVOICE";
    }
    this.receipt = data.receipt;
    this.invoice_id = data.invoice_id;
    this.payment_id = data.payment_id;
    this.created_at = dayjs(data.created_at);
  }

  /**
   * @param {Object} data
   * @param {number} data.id
   * @param {string} [data.receipt]
   * @param {string} data.invoice_id
   * @param {number} data.payment_id
   * @param {string} data.created_at
   *
   * @returns {Promise<Receipt>}
   */
  static async CREATE_NEW_RECEIPT(data) {
    data.created_at = dayjs().format("YYYY-MM-DD HH:mm:ss");
    return Receipts()
      .insert(data, "*")
      .then((result) => {
        if (result.length !== 1) {
          return Promise.reject("Error creating Receipt");
        }
        return new Receipt(result[0]);
      });
  }

  /**
   * 
   * @param {int} private_payment_reference 
   * @returns {Promise<Receipt>}
   */
  static async LOAD_FROM_PAYMENT_REFERENCE(private_payment_reference) {
    return Receipts().where("payment_id", private_payment_reference).first().then(receipt => {
      if (receipt == null) {
        return Promise.reject("Receipt not yet generated");
      }
      return new Receipt(receipt);
    });
  }

  /**
   * 
   * @param {number} internal_id 
   * @returns {Promise<Receipt>}
   */
  static async LOAD_RECEIPT_FROM_INTERNAL_ID(internal_id) {
    return Receipts().where("id", internal_id).first().then(receipt_data => {
      if (receipt_data !== undefined) {
        return new Receipt(receipt_data);
      }
      return Promise.reject("Cannot find Receipt with id " + internal_id);
    })
  }

  /**
   * Loads the latest receipt for a given array of paymentreferences
   * 
   * @param {array} payment_reference_ids 
   * @return {Promise<Receipt|null>}
   */
  static async GET_LATEST_RECEIPT(payment_reference_ids) {
    return __database_client("payments").whereIn("payment_reference_id", payment_reference_ids).rightJoin("receipts", "payments.receipt_id", "receipts.id").whereNotNull("receipts.invoice_id").orderBy("receipts.created_at", "desc").first().then(payment => {
      if (payment == undefined) {
        return null;
      }
      return new Receipt(payment);
    });
  }

  /**
   *
   * @param {string} receiptdata
   *
   * @returns {Promise<Receipt>}
   */
  async attachReceipt(receiptdata) {
    return Receipts()
      .where("id", this.id)
      .update({ receipt: receiptdata }, "*")
      .then((result) => {
        if (result && result.length === 1) {
          return new Receipt(result[0]);
        }
        return Promise.reject("Failed to insert receipt");
      });
  }
}

module.exports = Receipt;
