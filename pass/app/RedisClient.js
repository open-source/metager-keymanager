const config = require("config");
const Redis = require("ioredis");

class RedisClient {

    /**
     * 
     * @returns {Redis.Redis}
     */
    static CLIENT = () => {
        // Depending on configuration a redis client can be received directly by hostname
        // or via sentinel server(s)
        let redis_config = config.get("redis");
        let redis_client;
        if (redis_config.sentinel.active) {
            redis_client = new Redis({
                name: redis_config.sentinel.name,
                sentinelPassword: redis_config.sentinel.sentinelPassword,
                sentinels: redis_config.sentinel.sentinel_hosts,
                password: redis_config.password,
                db: 0
            });
        } else {
            redis_client = new Redis({
                host: redis_config.host,
                port: redis_config.port,
                password: redis_config.password,
                db: 0
            });
        }
        redis_client.on('error', error => {
            if (error.code === 'ECONNRESET') {
                console.error('Connection to Redis Session Store timed out.');
            } else if (error.code === 'ECONNREFUSED') {
                console.error('Connection to Redis Session Store refused!');
            } else console.error(error);
        });

        redis_client.on('reconnecting', error => {
            if (redis_client.status === 'reconnecting')
                console.info('Reconnecting to Redis Session Store...');
            else console.error('Error reconnecting to Redis Session Store.');
        });

        redis_client.on('connect', error => {
            if (!error) console.info('Connected to Redis Session Store!');
        });

        return redis_client;
    }
}
module.exports = RedisClient;