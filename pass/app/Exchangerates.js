const { writeFileSync, readFileSync, existsSync } = require("fs");
const dayjs = require("dayjs");
const config = require("config");

class Exchangerates {
    #data_path = "/data/exchangerates.json";
    #currencies = [
        "USD",
        "CAD",
        "GBP"
    ];

    #exchangerates = {

    };

    constructor() {
        if (existsSync(this.#data_path)) {
            this.#exchangerates = JSON.parse(readFileSync(this.#data_path));
        }
    }

    async update() {
        let currentdate = dayjs();
        let date_formatted = currentdate.format("YYYY-MM-DD");
        if (!(date_formatted in this.#exchangerates)) {
            let api_url = new URL(config.get("payments.exchange_api_apilayer.host"));
            let exchangerate = {}
            for (let i = 0; i < this.#currencies.length; i++) {

                let params = new URLSearchParams({
                    symbols: "EUR",
                    base: this.#currencies[i]
                });
                api_url.search = params.toString();
                await fetch(api_url).then(result => result.json()).then(result => {
                    exchangerate[this.#currencies[i]] = result.rates.EUR;
                }).catch(reason => {
                    console.error("reason");
                    exchangerate = null;
                })
                if (exchangerate === null) {
                    break;
                }
            }
            if (exchangerate !== null) {
                this.#exchangerates[date_formatted] = exchangerate;
                this.save();
            }
        }
    }

    save() {
        writeFileSync(this.#data_path, JSON.stringify(this.#exchangerates));
    }
}

module.exports = Exchangerates;