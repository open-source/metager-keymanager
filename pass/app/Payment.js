const Knex = require("knex");
const Redis = require("ioredis");
const config = require("config");
const Key = require("../app/Key");
const dayjs = require("dayjs");
const PaymentProcessor = require("./payment_processor/PaymentProcessor");

/**
 * @typedef {Object} PaymentsDB
 * @property {number} PaymentsDB.id
 * @property {number} PaymentsDB.price
 * @property {number} PaymentsDB.converted_price
 * @property {string} PaymentsDB.converted_currency
 * @property {number} PaymentsDB.payment_reference_id
 * @property {number} PaymentsDB.payment_processor_id
 * @property {string} PaymentsDB.payment_processor_data
 * @property {number} PaymentsDB.receipt_id
 * @property {string} PaymentsDB.created_at
 */
/** @returns {Knex.QueryBuilder<PaymentsDB, {}>} */
const Payments = () => __database_client("payments");

class Payment {
  /**
   *
   * @param {Object} payment_data
   * @param {number} payment_data.id
   * @param {number} payment_data.price
   * @param {number} payment_data.converted_price
   * @param {string} payment_data.converted_currency
   * @param {number} payment_data.payment_reference_id
   * @param {string} payment_data.payment_processor
   * @param {string} payment_data.payment_processor_id
   * @param {string} payment_data.payment_processor_data
   * @param {number} payment_data.receipt_id
   * @param {string} payment_data.created_at
   */
  constructor(payment_data) {
    this.id = payment_data.id;
    this.public_id =
      "A" + (payment_data.id + config.get("price.number_range.payments"));
    this.price = parseFloat(payment_data.price);
    this.converted_price = parseFloat(payment_data.converted_price);
    this.converted_currency = payment_data.converted_currency;
    this.payment_reference_id = payment_data.payment_reference_id;
    if (payment_data.payment_processor) {
      this.payment_processor = PaymentProcessor.LOAD_PAYMENT_PROCESSOR(
        payment_data.payment_processor,
        payment_data.payment_processor_data
      );
    }
    this.payment_processor_id = payment_data.payment_processor_id;
    if (payment_data.payment_processor_data) {
      this.payment_processor_data = payment_data.payment_processor_data;
    }
    this.receipt_id = payment_data.receipt_id;
    this.created_at = dayjs(payment_data.created_at);
  }

  /**
   *
   * @param {number} receipt_id
   *
   * @returns {Promise<Payment>}
   */
  async setReceipt(receipt_id) {
    return Payments()
      .where("id", this.id)
      .where("receipt_id", null)
      .update({ receipt_id: receipt_id }, ["*"])
      .then((result) => {
        if (result && result.length === 1) {
          return new Payment(result[0]);
        }
        return Promise.reject("Failed to Update Payment");
      });
  }

  /**
   * Returns the Netto Amount of the Payment
   *
   * @returns {number}
   */
  getBruttoPrice() {
    console.log(this.price);
    return this.price.toFixed(2);
  }

  /**
   * Returns the Netto Amount of the Payment
   *
   * @returns {number}
   */
  getNettoPrice() {
    let vat = this.getVat() / 100; // Vat is configured in %
    return (this.price / (1 + vat)).toFixed(2);
  }

  /**
   * Returns the VAT Amount of the Payment
   *
   * @returns {number}
   */
  getVatPrice() {
    let vat = this.getVat() / 100; // Vat is configured in %
    return ((this.price / (1 + vat)) * vat).toFixed(2);
  }

  /**
   * Returns the VAT Amount of the Payment
   *
   * @returns {number}
   */
  getVat() {
    return config.get("price.vat");
  }

  /**
   * Whether or not the Payment Processor allows refunding
   *
   * @returns {boolean}
   */
  isRefundAllowed() {
    if (!config.get("app.zammad.refund_enabled")) {
      return false;
    }
    // Check if Payment is older than 30 days
    let minDate = dayjs().add(-30, "days");
    if (this.created_at.isBefore(minDate)) {
      return false;
    }
    return this.payment_processor.isRefundSupported();
  }

  /**
   * @typedef {Object} InsertPaymentResult
   * @property {number} id Payment ID created
   * @property {number} price Booked price in EUR
   */
  /**
   * Creates a new Payment
   *
   * @param {Object} data
   * @param {number} data.price
   * @param {number} data.converted_price
   * @param {string} data.converted_currency
   * @param {number} data.payment_reference_id
   * @param {string} data.payment_processor
   * @param {string} [data.payment_processor_id]
   * @param {Object} [data.payment_processor_data]
   *
   *
   * @returns {Promise<Payment>} payment_data
   */
  static async CREATE_NEW_PAYMENT(data) {
    let insert_data = {};
    Object.assign(insert_data, data);
    if ("payment_processor_data" in data) {
      insert_data.payment_processor_data = JSON.stringify(
        data.payment_processor_data
      );
    }
    insert_data.created_at = dayjs().format("YYYY-MM-DD HH:mm:ss");
    return Payments()
      .insert(insert_data, "*")
      .then(async (result) => {
        if (result.length !== 1) {
          return Promise.reject("Error creating payment.");
        }

        return new Payment(result[0]);
      });
  }

  /**
   * 
   * @param {String} payment_id 
   * @returns {Promise<Payment|never>}
   */
  static async LOAD_FROM_PUBLIC_ID(payment_id) {
    let matcher = payment_id.match(/^(A)?(\d+)$/);
    if (!matcher) {
      return Promise.reject("Invalid Payment ID");
    }
    payment_id =
      parseInt(matcher[2]) - config.get("price.number_range.payments");
    return Payments()
      .where("id", payment_id)
      .first()
      .then((result_data) => {
        if (result_data === undefined) {
          return Promise.reject("Cannot find Payment");
        }
        return new Payment(result_data);
      });
  }

  /**
   * Returns all Payments for the given token reference that matches
   * for the key
   *
   * @param {string} payment_reference
   * @param {string} key
   *
   * @returns {Promise<Payment[]>}
   */
  static async LOAD_PAYMENTS_FROM_REFERENCE(internal_payment_reference, key) {
    return Payments()
      .select("payments.*")
      .leftJoin(
        "payment_references",
        "payments.payment_reference_id",
        "=",
        "payment_references.id"
      )
      .where("payment_references.key", key)
      .andWhere("payment_references.id", internal_payment_reference)
      .orderBy("created_at", "desc")
      .then((payment_data) => {
        let payments = [];
        for (let i = 0; i < payment_data.length; i++) {
          payments.push(new Payment(payment_data[i]));
        }
        return payments;
      });
  }
}

module.exports = Payment;
