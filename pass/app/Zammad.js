const config = require("config");

class Zammad {

    /**
     * Creates a notification in the configured Zammad instance
     * 
     * @param {number} token_amount
     * @param {number} $price
     * @param {string} $payment_method
     * 
     * @returns {Promise<boolean>}
     */
    static async CREATE_NOTIFICATION(token_amount, price, payment_method) {
        // Check if notifications are turned on
        try {
            if (config.get("app.zammad.notification_enabled")) {
                let zammad_url = config.get("app.zammad.url") + "/api/v1/ticket_articles";

                let zammad_data = {
                    "ticket_id": config.get("app.zammad.notification_ticket_id"),
                    "subject": "Neue Bestellung",
                    "body": `Tokens: ${token_amount}\nBetrag: ${price.toFixed(2)}€\nZahlungsart: ${payment_method}`,
                    "content_type": "text/plain",
                    "type": "web",
                    "internal": false,
                    "sender": "Agent"
                }
                return fetch(zammad_url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Token token=${config.get("app.zammad.api_key")}`
                    },
                    body: JSON.stringify(zammad_data)
                }).then(response => {
                    return true;
                }).catch(e => {
                    console.error(e);
                    return false;
                })
            } else {
                return true;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    /**
     * Creates a refund ticket in the configured Zammad instance
     * 
     * @param {string} message
     * 
     * @returns {Promise<boolean>}
     */
    static async CREATE_REFUND_TICKET(message, payment_reference_id) {
        // Check if notifications are turned on
        try {
            if (config.get("app.zammad.refund_enabled")) {
                let zammad_url = config.get("app.zammad.url") + "/api/v1/tickets";
                let subject = `Erstattung (${payment_reference_id})`;
                let zammad_data = {
                    "title": subject,
                    "group": "Tokens",
                    "customer_id": "guess:noreply@metager.de",
                    "preferences": { "channel_id": 3 },
                    "article": {
                        "type": "note",
                        "sender": "Customer",
                        "subject": subject,
                        "body": message,
                        "content_type": "text/html",
                        "internal": false
                    }
                }
                return fetch(zammad_url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Token token=${config.get("app.zammad.api_key")}`
                    },
                    body: JSON.stringify(zammad_data)
                }).then(response => {
                    return true;
                }).catch(e => {
                    console.error(e);
                    return false;
                })
            } else {
                return true;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    /**
     * Creates a receipt ticket in the configured Zammad instance
     * 
     * @param {string} name
     * @param {string} email
     * @param {string} message
     * @param {string} payment_id
     * 
     * @returns {Promise<boolean>}
     */
    static async CREATE_RECEIPT_TICKET(name, email, message, payment_id) {
        // Check if notifications are turned on
        try {
            if (config.get("app.zammad.receipt_enabled")) {
                let zammad_url = config.get("app.zammad.url") + "/api/v1/tickets";
                let subject = `Rechnung (${payment_id})`;
                let zammad_data = {
                    "title": subject,
                    "group": "Tokens",
                    "customer_id": `guess:${email}`,
                    "preferences": { "channel_id": 3 },
                    "article": {
                        "type": "email",
                        "sender": "Customer",
                        "from": `${name} <${email}>`,
                        "reply_to": email,
                        "to": "support@metager.de",
                        "subject": subject,
                        "body": message,
                        "content_type": "text/html",
                        "internal": false
                    }
                }
                return fetch(zammad_url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Token token=${config.get("app.zammad.api_key")}`
                    },
                    body: JSON.stringify(zammad_data)
                }).then(response => {
                    return true;
                }).catch(e => {
                    console.error(e);
                    return false;
                })
            } else {
                return true;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    }
}

module.exports = Zammad;