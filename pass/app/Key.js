const dayjs = require("dayjs");
const config = require("config");
const RedisClient = require("./RedisClient");

class Key {
  static get DATABASE_PREFIX() {
    return "key:";
  }

  static get MAX_ORDERS_PER_KEY() {
    return 3;
  }

  static get EXPIRATION_AFTER_CHARGE_DAYS() {
    return 365;
  }

  /**
   * @type {String}
   */
  #key;

  /**
   * @type {Boolean}
   */
  #writable;
  /**
   * @typedef {Object} KeyOrder
   * @property {Number} amount
   * @property {dayjs.Dayjs} expiration
   * @property {string} payment_reference_id
   */
  /**
   * @type {Array<KeyOrder>}
   */
  #key_orders = [];

  /**
   * Contains changes for the given key before saving
   *
   * @typedef {Object} KeyChange
   * @property {Number} payment_reference_id
   * @property {Number} charge_change Can be positive or negative
   */
  /**
   * @type {Array<KeyChange>}
   */
  #changes = [];

  /**
   *
   * @param {String} key
   * @param {Array<KeyOrder>} key_data
   * @param {Boolean} writable
   */
  constructor(key, key_data, writable = false) {
    this.#key = key;
    this.#writable = writable;
    this.#key_orders = key_data;
  }

  /**
   * Returns the string representation of this key
   *
   * @returns {String}
   */
  get_key() {
    return this.#key;
  }

  /**
   * Returns a short temporary logincode to be used
   * 
   * @returns {String}
   */
  async get_logincode() {
    let prefix = "logincode:";
    let expiration = 10; // Code will be valid for 10 seconds

    // Check if there is already a code for this key
    let code = await __redis_client.get(prefix + this.#key);

    if (code == null) {
      let max = 999999;
      let min = 100000;
      let validcode = false;

      while (!validcode) {
        code = Math.floor(Math.random() * (max - min - 1)) + min;
        code = code + "";
        validcode = await __redis_client.setnx(prefix + code, this.#key).then(result => {
          if (result == 1) {
            return true;
          } else {
            return false;
          }
        });
      }
      await __redis_client.expire(prefix + code, expiration);
      await __redis_client.setex(prefix + this.#key, expiration, code);
    } else {
      // Refresh expiration time
      await __redis_client.expire(prefix + code, expiration);
      await __redis_client.expire(prefix + this.#key, expiration);
    }

    return code;
  }

  /**
   * Returns the sum of all stored key orders
   *
   * @param {String} order_id - Optional
   *
   * @returns {Number}
   */
  get_charge(payment_reference_id = null) {
    let sum = 0;
    this.#key_orders.forEach((key_order) => {
      if (
        payment_reference_id === null ||
        payment_reference_id === key_order.payment_reference_id
      ) {
        sum += key_order.amount;
      }
    });
    return sum;
  }

  /**
   * Returns the date when all order keys will have been expired
   *
   * @returns {dayjs.Dayjs}
   */
  get_expiration_date() {
    /**
     * @type {dayjs.Dayjs}
     */
    let expiration = null;
    this.#key_orders.forEach((key_order) => {
      if (expiration === null) {
        expiration = key_order.expiration;
      } else if (expiration.isAfter(key_order.expiration)) {
        expiration = key_order.expiration;
      }
    });
    if (expiration === null) {
      expiration = dayjs().add(config.get("keys.expiration_years"), "year");
    }
    return expiration;
  }

  /**
   *
   * @returns {KeyOrder[]}
   */
  get_charge_orders() {
    return this.#key_orders;
  }

  /**
   * Charges this key in response to an order
   *
   * @param {Number} amount
   * @param {Number} payment_reference_id
   * @param {dayjs.Dayjs} expiration
   */
  charge_key_order(amount, payment_reference_id, expiration) {
    let order_charged = false;
    // If we recharge an existing order
    this.#key_orders.forEach((key_order) => {
      if (key_order.payment_reference_id === payment_reference_id) {
        key_order.amount += amount;
        order_charged = true;
        this.#changes.push({
          payment_reference_id: payment_reference_id,
          timestamp: dayjs(),
          charge_change: amount,
        });
      }
    });
    // Otherwise create new Order
    if (!order_charged) {
      this.#key_orders.push({
        amount: amount,
        payment_reference_id: payment_reference_id,
        expiration: expiration,
      });
      this.#changes.push({
        payment_reference_id: payment_reference_id,
        timestamp: dayjs(),
        charge_change: amount,
      });
    }
  }

  /**
   * Discharges this key for a specific order
   *
   * @param {Number} amount
   * @param {Number} payment_reference_id
   *
   * @returns {Number} Amount discharged
   */
  discharge_key(amount, payment_reference_id = null) {
    let discharged_amount = null;
    this.#key_orders.every((key_order, index) => {
      if (
        payment_reference_id === null ||
        key_order.payment_reference_id === payment_reference_id
      ) {
        if (discharged_amount === null) {
          discharged_amount = 0;
        }
        let missing_amount = parseFloat((amount - discharged_amount).toFixed(1));
        if (missing_amount >= key_order.amount) {
          // Key cannot be reduced by more than its current charge
          discharged_amount += key_order.amount;
          this.#key_orders[index].amount = 0;
          this.#changes.push({
            payment_reference_id: key_order.payment_reference_id,
            timestamp: dayjs(),
            token_change: key_order.amount * -1,
          });
        } else {
          discharged_amount += missing_amount;
          this.#key_orders[index].amount = parseFloat((this.#key_orders[index].amount - missing_amount).toFixed(1));
          this.#changes.push({
            payment_reference_id: key_order.payment_reference_id,
            timestamp: dayjs(),
            token_change: missing_amount * -1,
          });
        }
        if (amount - discharged_amount === 0) {
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    });
    if (discharged_amount !== null) {
      return discharged_amount;
    } else {
      return 0;
    }
  }

  isChargable() {
    if (this.#key_orders.length >= Key.MAX_ORDERS_PER_KEY) {
      return false;
    } else {
      return true;
    }
  }

  async save() {
    if (!this.#writable) {
      throw new Error("Cannot write to readonly Key");
    }

    // Remove empty Orders
    let new_key_orders = [];
    this.#key_orders.forEach((key_order) => {
      if (key_order.amount > 0 && key_order.expiration.isAfter(dayjs())) {
        new_key_orders.push(key_order);
      }
    });
    this.#key_orders = new_key_orders;

    let write_lock_key = Key.DATABASE_PREFIX + this.#key + ":writelock";
    let redis_commands = __redis_client
      .pipeline()
      .setex(
        Key.DATABASE_PREFIX + this.#key,
        this.get_expiration_date().diff(dayjs(), "second"),
        JSON.stringify(this.#key_orders)
      )
      .del(write_lock_key);

    for (let i = 0; i < this.#changes.length; i++) {
      let change = this.#changes[i];
      redis_commands = redis_commands.rpush(
        "key_changes",
        JSON.stringify(change)
      );
    }

    return redis_commands.exec().then(() => {
      this.#changes = [];
      return this;
    });
  }

  /**
   * Resolves with a Key object
   *
   * @param {string} key
   * @param {boolean} writable
   *
   * @return {Promise<Key>}
   */
  static async GET_KEY(key, writable = false) {
    // Check if supplied key is UUID v4
    if (!Key.IS_VALID_UUID(key)) {
      if (typeof key != "string") {
        key = "";
      }

      const { createHash } = await import("node:crypto");
      let hash = createHash("md5");
      hash.update(key);

      // According to the definition of UUIDv4 the 13th digit needs to specify the uuid version (i.e. 4 in our case)
      // Additionally the 17th digit needs to specify its variant (DCE 1.1, ISO/IEC 11578:1996 in our case)
      // This means the binary representation of the 17th digit needs to be 10xx with the x's being random
      // This means we need a decimal number between 8 and 11
      let variant = 0;
      for (let i = 0; i < key.length; i++) {
        variant += key.charCodeAt(i);
      }
      variant = variant % 4;
      // Convert decimal to hex
      variant = 8 + variant;
      variant = variant.toString(16);
      key = hash
        .copy()
        .digest("hex")
        .replace(
          /^([a-f0-9]{8})([a-f0-9]{4})[a-f0-9]{1}([a-f0-9]{3})[a-f0-9]{1}([a-f0-9]{3})([a-f0-9]{12})/,
          "$1-$2-4$3-" + variant + "$4-$5"
        );
    }
    return __redis_client
      .get(Key.DATABASE_PREFIX + key)
      .then((key_data) => {
        if (!key_data) {
          return [];
        }
        key_data = JSON.parse(key_data);
        key_data.forEach((key_order, index) => {
          key_data[index].expiration = dayjs(key_order.expiration);
        });
        return key_data;
      })
      .then(async (key_data) => {
        if (writable) {
          // Acquire an exclusive lock for this key entry
          await Key.GET_WRITELOCK(key);
        }
        return new Key(key, key_data, writable);
      });
  }

  static async GET_WRITELOCK(key) {
    let write_lock_key = Key.DATABASE_PREFIX + key + ":writelock";

    return new Promise(async (resolve, reject) => {
      let timestart = dayjs();
      let write_lock = 0;
      do {
        write_lock = await __redis_client
          .pipeline()
          .setnx(write_lock_key, 1)
          .expiretime(write_lock_key)
          .expire(write_lock_key, 15)
          .exec();
        let expire_at = write_lock[1][1];
        write_lock = write_lock[0][1];
        if (write_lock === 1) {
          resolve(true);
          break;
        } else if (dayjs().diff(timestart, "second") >= 15) {
          if (expire_at >= 0) {
            await __redis_client.expireat(write_lock_key, expire_at);
          }
          reject("Timed out waiting for write lock for Key");
          break;
        } else {
          if (expire_at >= 0) {
            await __redis_client.expireat(write_lock_key, expire_at);
          }
          await new Promise((resolve) => setTimeout(resolve, 1000));
        }
      } while (true);
    });
  }

  static async EXISTS(key) {
    return new Promise((resolve, reject) => {
      __redis_client.exists(Key.DATABASE_PREFIX + key).then((result) => {
        resolve(result);
      });
    });
  }

  static IS_VALID_UUID(input_key) {
    if (!input_key || typeof input_key != "string") return false;
    return input_key.match(
      /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
    );
  }

  /**
   *
   * @returns {Promise<Key>} A new MetaGer-Pass Key with 0 searches
   */
  static async GET_NEW_KEY() {
    let crypto = require("crypto");
    do {
      let key = crypto.randomUUID();

      let key_exists = await __redis_client.exists(Key.DATABASE_PREFIX + key);
      if (!key_exists) {
        return Key.GET_KEY(key);
      }
      await new Promise((resolve) => setTimeout(resolve, 50));
    } while (true);
  }
}

module.exports = Key;
