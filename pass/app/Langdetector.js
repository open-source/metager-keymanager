module.exports = {
    name: 'mg_detection',
    lookup: (req, res, options) => {
        // Cookie is checked at this point
        // Next detection in order is the request path
        let path = req.path.replace(/^\/+/, "").replace(/\/+$/, "").split("/");
        let lang_matches = path[0].match(/^([a-z]{2})-([A-Z]{2})/);
        if (!lang_matches) {
            // Check if a lang cookie is defined
            let cookie = req.cookies["web_setting_m"];
            if (cookie && cookie.match()) {
                lang_matches = cookie.match(/^([a-z]{2})-([A-Z]{2})/);
            }
        }
        if (path.length > 0 && lang_matches) {
            let path_tool = require("path");
            let fs = require("fs");
            let lang_folder = path_tool.join(__dirname, "../lang", lang_matches[0]);
            // Check if translation exists for full locale
            if (fs.existsSync(lang_folder)) {
                return lang_matches[0];
            }
            // Check if translation exists for language part of locale
            lang_folder = path_tool.join(__dirname, "../lang", lang_matches[1]);
            if (fs.existsSync(lang_folder)) {
                return lang_matches[0];
            }
        }

        // If the path does not match we'll try to guess a locale based on Accept-Language header
        let acceptLanguage = require("accept-language");
        acceptLanguage.languages([null, "de-DE", "de-AT", "de-CH", "de", "en-GB", "en-UK", "en-IE", "en-US", "en", "es-ES", "es-MX", "es"]);
        let guessed_lang = acceptLanguage.get(req.headers["accept-language"]);
        if (guessed_lang) {
            return guessed_lang;
        }

        // If all prior checks failed we'll use the default depending on the current domain
        let language = 'en';
        if (req.hostname === "metager.de") {
            language = 'de';
        }
        return language;
    },
    cacheUserLanguage: (req, res, lng, options) => {
        // options -> are passed in options
        // lng -> current language, will be called after init and on changeLanguage
        // store it
    }
}