const dayjs = require("dayjs");
const i18next = require("i18next");
const config = require("config");
const { fontSize } = require("pdfkit");
const Redis = require("ioredis");
const PaymentReference = require("../PaymentReference");
const Payment = require("../Payment");

class OrderReceipt {
  /**
   *
   * @param {PaymentReference} payment_reference
   * @param {Payment} payment
   * @param {Object} invoice
   * @param {i18next.TFunction} t
   */
  static async CREATE_ORDER_RECEIPT(
    payment_reference,
    payment,
    receipt = undefined,
    t
  ) {
    let letter_left_margin = OrderReceipt.CM_TO_POINTS(2.5, false);
    let letter_right_margin = OrderReceipt.CM_TO_POINTS(2, false);

    let PDFDocument = require("pdfkit");
    let title = t("title_order", { ns: "invoice", orderid: payment.public_id });
    if (receipt) {
      title = t("title_invoice", { ns: "invoice", orderid: receipt.public_id });
    }
    const doc = new PDFDocument({
      size: "A4",
      info: {
        Title: title,
        Author: t("author", { ns: "invoice" }),
        Subject: t("subject", {
          ns: "invoice",
          amount: Math.round(
            (payment.price / payment_reference.price) * payment_reference.amount
          ),
        }),
      },
    });

    doc
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
      .fontSize(10);

    // The header with logo and line
    doc.image("public/images/metager.png", letter_left_margin, 30, {
      height: 20,
    });
    doc.image("public/images/suma-ev.png", 400, 32, {
      height: 18,
    });
    doc
      .moveTo(letter_left_margin, 60)
      .lineTo(OrderReceipt.CM_TO_POINTS(21, false) - letter_right_margin, 60)
      .strokeColor("#515151")
      .stroke();

    // Our Address Information
    if (receipt) {
      doc
        .fontSize(8)
        .text(
          "SUMA-EV | Postfach 51 01 43 | 30631 Hannover | Deutschland",
          OrderReceipt.CM_TO_POINTS(2, false),
          OrderReceipt.CM_TO_POINTS(4.5, true),
          {
            width: OrderReceipt.CM_TO_POINTS(8.5, false),
          }
        )
        .moveDown()
        .fontSize(10)
        .text(receipt.company)
        .text(receipt.name);
      receipt.address.split("\r\n").forEach((line) => {
        doc.text(line);
      });
    }

    // General Information
    let text =
      (receipt
        ? t("invoice", { ns: "invoice" })
        : t("order", { ns: "invoice" })) + ":";
    let number = receipt ? receipt.public_id : payment.public_id;
    doc.fontSize(10);
    if (receipt) {
      doc
        .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
        .text(
          t("invoice", { ns: "invoice" }),
          OrderReceipt.CM_TO_POINTS(21 - 8.5, false),
          OrderReceipt.CM_TO_POINTS(5, true),
          {
            width: OrderReceipt.CM_TO_POINTS(7.5, false),
            continued: true,
            lineGap: 4,
          }
        )
        .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
        .text(receipt.public_id, { align: "right" })
        .text(t("order", { ns: "invoice" }), {
          width: OrderReceipt.CM_TO_POINTS(7.5, false),
          continued: true,
          lineGap: 4,
        })
        .text(payment.public_id, { align: "right" });
    } else {
      doc
        .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
        .text(
          t("order", { ns: "invoice" }),
          OrderReceipt.CM_TO_POINTS(21 - 8.5, false),
          OrderReceipt.CM_TO_POINTS(5, true),
          {
            width: OrderReceipt.CM_TO_POINTS(7.5, false),
            continued: true,
            lineGap: 4,
          }
        )
        .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
        .text(payment.public_id, { align: "right" });
    }
    doc
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
      .text(t("payment_reference_id", { ns: "invoice" }) + ":", {
        width: OrderReceipt.CM_TO_POINTS(7.5, false),
        lineGap: 4,
        continued: true,
      })
      .text(payment_reference.public_id, { align: "right" })
      .text(t("phone", { ns: "invoice" }) + ":", {
        width: OrderReceipt.CM_TO_POINTS(7.5, false),
        lineGap: 4,
        continued: true,
      })
      .text("+4951134000070", { align: "right" })
      .text("E-Mail:", {
        width: OrderReceipt.CM_TO_POINTS(7.5, false),
        lineGap: 4,
        continued: true,
      })
      .text(t("mail-address", { ns: "invoice" }), { align: "right" })
      .text("Internet:", {
        width: OrderReceipt.CM_TO_POINTS(7.5, false),
        lineGap: 4,
        continued: true,
      })
      .text(t("domain", { ns: "invoice" }), { align: "right" })
      .moveDown();

    if (receipt) {
      doc
        .text(t("invoice_date", { ns: "invoice" }) + ":", {
          width: OrderReceipt.CM_TO_POINTS(7.5, false),
          lineGap: 4,
          continued: true,
        })
        .text(dayjs().format("DD.MM.YYYY"), { align: "right" });
    }
    doc
      .text(t("purchase_date", { ns: "invoice" }) + ":", {
        width: OrderReceipt.CM_TO_POINTS(7.5, false),
        lineGap: 4,
        continued: true,
      })
      .text(payment.created_at.format("DD.MM.YYYY"), { align: "right" });

    // Actual content of the letter
    // Orange Line
    doc
      .fontSize(12)
      .moveTo(
        OrderReceipt.CM_TO_POINTS(2.5, false),
        OrderReceipt.CM_TO_POINTS(12.6, true)
      )
      .lineTo(
        OrderReceipt.CM_TO_POINTS(21 - 2, false),
        OrderReceipt.CM_TO_POINTS(12.6, true)
      )
      .strokeColor("#ff7f00")
      .stroke();

    text = receipt
      ? t("invoice", { ns: "invoice" })
      : t("order-confirmation", { ns: "invoice" });
    doc
      .fontSize(12)
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        text + " " + number,
        OrderReceipt.CM_TO_POINTS(2.5, false),
        OrderReceipt.CM_TO_POINTS(9.846, true)
      );

    doc
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        t("details", { ns: "invoice" }),
        OrderReceipt.CM_TO_POINTS(2.5, false),
        OrderReceipt.CM_TO_POINTS(12, true),
        {
          width: OrderReceipt.CM_TO_POINTS(10, false),
          align: "center",
        }
      )
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
      .moveDown()
      .text(t("product", { ns: "invoice" }), {
        width: OrderReceipt.CM_TO_POINTS(10, false),
        align: "right",
      });

    doc
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        t("count", { ns: "invoice" }),
        OrderReceipt.CM_TO_POINTS(12.5, false),
        OrderReceipt.CM_TO_POINTS(12, true),
        {
          width: OrderReceipt.CM_TO_POINTS(3.25, false),
          align: "center",
        }
      )
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
      .moveDown()
      .text(
        Math.round(
          (payment.price / payment_reference.price) * payment_reference.amount
        ),
        {
          width: OrderReceipt.CM_TO_POINTS(3.25, false),
          align: "right",
          lineGap: 8,
        }
      )
      .text(t("vat", { ns: "invoice", vat: config.get("price.vat") }), {
        width: OrderReceipt.CM_TO_POINTS(3.25, false),
        align: "right",
        lineGap: 8,
      })
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(t("total", { ns: "invoice" }), {
        width: OrderReceipt.CM_TO_POINTS(3.25, false),
        align: "right",
        lineGap: 8,
      })
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    if (payment.converted_currency !== "EUR") {
      doc
        .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
        .text(t("currency-exchange", { ns: "invoice" }), {
          width: OrderReceipt.CM_TO_POINTS(3.25, false),
          align: "right",
        })
        .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
        .fontSize(8)
        .text(
          " 1 " +
          payment.converted_currency +
          " = " +
          (payment.price / payment.converted_price).toFixed(6) +
          "€",
          {
            width: OrderReceipt.CM_TO_POINTS(3.25, false),
            align: "right",
          }
        )
        .fontSize(12);
    }

    doc
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        t("price", { ns: "invoice" }),
        OrderReceipt.CM_TO_POINTS(15.75, false),
        OrderReceipt.CM_TO_POINTS(12, true),
        {
          width: OrderReceipt.CM_TO_POINTS(3.25, false),
          align: "center",
        }
      )
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf")
      .moveDown()
      .text(payment.getNettoPrice() + " €", {
        width: OrderReceipt.CM_TO_POINTS(3.25, false),
        align: "right",
        lineGap: 8,
      })
      .text(payment.getVatPrice() + " €", {
        width: OrderReceipt.CM_TO_POINTS(3.25, false),
        align: "right",
        lineGap: 8,
      })
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(payment.getBruttoPrice() + " €", {
        width: OrderReceipt.CM_TO_POINTS(3.25, false),
        align: "right",
        lineGap: 8,
      })
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");

    if (payment.converted_currency !== "EUR") {
      doc
        .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
        .text(payment.converted_price + " " + payment.converted_currency, {
          width: OrderReceipt.CM_TO_POINTS(3.25, false),
          align: "right",
        })
        .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    }
    if (receipt) {
      doc
        .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
        .text(
          t("payment-received", { ns: "invoice" }),
          OrderReceipt.CM_TO_POINTS(2.5, false),
          OrderReceipt.CM_TO_POINTS(18, true),
          {
            align: "center",
          }
        )
        .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    }

    // Footer
    doc
      .moveTo(
        OrderReceipt.CM_TO_POINTS(2.5, false),
        OrderReceipt.CM_TO_POINTS(27.2, false)
      )
      .lineTo(
        OrderReceipt.CM_TO_POINTS(21 - 2, false),
        OrderReceipt.CM_TO_POINTS(27.2, true)
      )
      .strokeColor("#515151")
      .stroke();

    let textbox_width = doc.widthOfString("Vorstand");
    doc.fontSize(8);
    textbox_width = Math.max(
      doc.widthOfString("Dominik Hebeler", "Manuela Branz", "Carsten Riel")
    );
    doc.fontSize(10);

    let x = OrderReceipt.CM_TO_POINTS(21 - 2, false) - textbox_width;
    doc
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        t("board", { ns: "invoice" }),
        x,
        OrderReceipt.CM_TO_POINTS(27.3, true),
        {
          lineBreak: false,
        }
      )
      .fontSize(8)
      .moveDown(1.5)
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    doc.x = x;
    doc
      .text("Dominik Hebeler", {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc
      .text("Manuela Branz", {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc.text("Carsten Riel", {
      lineBreak: false,
    });

    textbox_width = doc.widthOfString(
      t("vatid", { ns: "invoice" }) + ": DE 300 464 091"
    );
    x = OrderReceipt.CM_TO_POINTS(21 / 2, false) - textbox_width / 2;

    doc
      .fontSize(10)
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text(
        t("registrar", { ns: "invoice" }),
        x,
        OrderReceipt.CM_TO_POINTS(27.3, true),
        {
          lineBreak: false,
        }
      )
      .fontSize(8)
      .moveDown(1.5)
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    doc.x = x;
    doc
      .text(t("registered-at", { ns: "invoice" }), {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc
      .text("Nummer: VR200033", {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc.text(t("vatid", { ns: "invoice" }) + ": DE 300 464 091", {
      lineBreak: false,
    });

    x = OrderReceipt.CM_TO_POINTS(2.5, false);
    doc
      .fontSize(10)
      .font("public/fonts/liberation-sans/LiberationSans-Bold.ttf")
      .text("SUMA-EV", x, OrderReceipt.CM_TO_POINTS(27.3, true), {
        lineBreak: false,
      })
      .fontSize(8)
      .moveDown(1.5)
      .font("public/fonts/liberation-sans/LiberationSans-Regular.ttf");
    doc.x = x;
    doc
      .text("Postfach 51 01 43", {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc
      .text("30631 Hannover", {
        lineBreak: false,
      })
      .moveDown();
    doc.x = x;
    doc.text("Deutschland", {
      lineBreak: false,
    });

    doc.end();

    const chunks = [];
    return new Promise((resolve, reject) => {
      doc.addListener("data", (chunk) => chunks.push(chunk));
      doc.addListener("error", (err) => reject(err));
      doc.addListener("end", () => resolve(chunks));
    });
  }

  /**
   *
   * @param {Number} cm
   * @param {boolean} height
   * @returns
   */
  static CM_TO_POINTS(cm, vertical) {
    let width_in_points = 595.28;
    let height_in_points = 841.89;
    if (vertical) {
      return cm * (height_in_points / 29.7);
    } else {
      return cm * (width_in_points / 21.0);
    }
  }

  static async CREATE_UNIQUE_RECEIPT_ID() {
    let invoice_id = null;
    let redis_client = /** @type {Redis.Redis} */ (__redis_client);

    do {
      let current_time = dayjs();
      let redis_lock = "lock:inv:" + current_time.unix();
      let unique = await redis_client.setnx(redis_lock, 1);
      await redis_client.expire(redis_lock, 1);
      if (unique === 1) {
        invoice_id = current_time.unix();
        break;
      } else {
        await new Promise((resolve) => {
          setTimeout(resolve, 1000);
        });
      }
    } while (true);
    return invoice_id;
  }
}

module.exports = OrderReceipt;
