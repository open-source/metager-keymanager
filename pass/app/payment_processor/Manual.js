const PaymentProcessor = require("./PaymentProcessor");
const i18next = require("i18next");

class Manual extends PaymentProcessor {
  #note;

  static get NAME() {
    return "Manual";
  }

  /**
   *
   * Constructs a new manual Payment
   * No real payment is executed here but you can leave a note
   * describing the source/reason of this Payment
   *
   * @param {String} note
   */
  constructor(note = "") {
    super();

    this.#note = note;
  }

  /**
   *
   * @param {PaymentReference} payment_reference
   * @param {i18next.TFunction} t
   */
  async createOrder(payment_reference, t) {
    return Promise.resolve();
  }

  async captureOrder() {
    return Promise.resolve();
  }
  /**
   *
   * @returns {boolean}
   */
  isRefundSupported() {
    return false;
  }
  async refundOrder() {
    throw new Error("Manual Payments cannot be refunded");
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {Object}
   */
  serialize() {
    return {
      processor_name: "Manual",
      note: this.#note,
    };
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {PaymentProcessor}
   */
  static DESERIALIZE(data) {
    return new Manual(data.note);
  }
}

module.exports = Manual;
