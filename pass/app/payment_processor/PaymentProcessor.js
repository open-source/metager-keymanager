const i18next = require("i18next");
const PaymentReference = require("../PaymentReference");

class PaymentProcessor {
  static get NAME() {
    throw new Error("Function NAME() must be implemented");
  }
  constructor() {
    if (this.constructor === PaymentProcessor) {
      throw new Error("Cannot instantiate abstract class");
    }
  }

  /**
   *
   * @param {PaymentReference} payment_reference
   * @param {i18next.TFunction} t
   */
  async createOrder(payment_reference, t) {
    throw new Error("Function createOrder() must be implemented");
  }

  async captureOrder() {
    throw new Error("Function captureOrder() must be implemented");
  }
  /**
   *
   * @returns {boolean}
   */
  isRefundSupported() {
    throw new Error("Function isRefundPossible() must be implemented");
  }
  async refundOrder() {
    throw new Error("Function refundOrder() must be implemented");
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {Object}
   */
  serialize() {
    throw new Error("Function serialize() must be implemented");
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {PaymentProcessor}
   */
  static DESERIALIZE() {
    throw new Error("Function deserialize() must be implemented");
  }

  /**
   * Turns serialized data into class object of type PaymentProcessor
   *
   * @param {string} processor_name
   * @param {string} serialized_data
   *
   * @returns {PaymentProcessor}
   */
  static LOAD_PAYMENT_PROCESSOR(processor_name, serialized_data) {
    let processors = {
      Paypal: require("./Paypal"),
      Manual: require("./Manual"),
      Micropayment: require("./Micropayment"),
      Cash: require("./Cash"),
    };

    if (!(processor_name in processors)) {
      throw new Error(`Cannot find Payment Processor ${processor_name}`);
    }
    return processors[processor_name].DESERIALIZE(serialized_data);
  }
}

module.exports = PaymentProcessor;
