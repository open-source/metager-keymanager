const PaymentProcessor = require("./PaymentProcessor");
const i18next = require("i18next");
const config = require("config");
const PaymentReference = require("../PaymentReference");

class Micropayment extends PaymentProcessor {
  static get NAME() {
    return "Micropayment";
  }
  /**
   * @var {string[]}
   */
  #supported_services = ["sofort", "prepay", "lastschrift", "directbanking"];

  /**
   * @var {string}
   */
  #service;

  /**
   * @var {URL}
   */
  #data;

  /**
   *
   * @param {string} service
   */
  constructor(service = "paysafecard", data = null) {
    super();

    if (this.#supported_services.indexOf(service) === -1) {
      throw new Error(`Payment Service ${service} is not supported.`);
    }
    this.#service = service;
    this.#data = data;
  }

  /**
   * Generates a URL for the customer where he is redirected to
   * to complete Payment.
   *
   * @param {PaymentReference} payment_reference
   * @param {i18next.TFunction} t
   *
   * @returns {Promise<URL>}
   */
  async createOrder(payment_reference, t) {
    return Promise.resolve();
  }

  /**
   * Generates a URL for the customer to be redirected to after Payment
   *
   * @returns {Promise<URL>}
   */
  async captureOrder() {
    return Promise.resolve();
  }

  /**
   *
   * @returns {boolean}
   */
  isRefundSupported() {
    return true;
  }

  async refundOrder() {
    throw new Error("Manual Payments cannot be refunded");
  }

  static async VERIFY_WEBHOOK(query) {
    // Verify that secretkey is correct
    if (
      !("secretfield" in query) ||
      query.secretfield !== config.get("payments.micropayment.secretfield")
    ) {
      return Promise.reject("Secretfield does not match");
    }
    if (
      !("testmode" in query) ||
      (query.testmode === "1" && process.env.NODE_ENV !== "development")
    ) {
      return Promise.reject("Testmode Payment not accepted in production");
    }
    if (!("currency" in query) || query.currency !== "EUR") {
      return Promise.reject("Can only receive Payments in EUR");
    }
    return Promise.resolve();
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {Object}
   */
  serialize() {
    return {
      processor_name: "Micropayment",
      service: this.#service,
      data: this.#data,
    };
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {PaymentProcessor}
   */
  static DESERIALIZE(data) {
    data = JSON.parse(data);
    return new Micropayment(data.service, data);
  }
}

module.exports = Micropayment;
