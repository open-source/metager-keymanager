const PaymentReference = require("../PaymentReference");
const Payment = require("../Payment");
const dayjs = require("dayjs");
const config = require("config");
const PaymentProcessor = require("./PaymentProcessor");
const webhook_redis_key = "checkout_paypal_webhook_id";
const base = config.get(`payments.paypal.base`);
const i18next = require("i18next");

class Paypal extends PaymentProcessor {
  static get NAME() {
    return "Paypal";
  }
  /**
   *
   * @returns String
   */
  #client_id = config.get(`payments.paypal.client_id`);
  #app_secret = config.get(`payments.paypal.secret`);
  #base = config.get(`payments.paypal.base`);

  #order_id;
  #order_status;

  constructor(order_id = null, order_status = null) {
    super();

    if (order_id) {
      this.#order_id = order_id;
    }
    if (order_status) {
      this.#order_status = order_status;
    }
  }

  getOrderId() {
    return this.#order_id;
  }
  getOrderStatus() {
    return this.#order_status;
  }

  /**
   *
   * @returns {boolean}
   */
  isRefundSupported() {
    return true;
  }

  /**
   *
   * @param {PaymentReference} payment_reference
   * @param {boolean} creditcard_payment
   * @param {i18next.TFunction} t
   *
   * @returns {Promise<Object>}
   */
  async createOrder(payment_reference, creditcard_payment = false, t) {
    let payment = new Payment({
      id: -1,
      price: payment_reference.price,
      converted_price: payment_reference.price,
      converted_currency: "EUR",
      payment_reference_id: 0,
      created_at: dayjs().format("YYYY-MM-DD HH:mm:ss"),
    });

    let paypal_order = {
      intent: "CAPTURE",
      purchase_units: [
        {
          invoice_id: payment_reference.public_id,
          custom_id: payment_reference.public_id,
          description: t("product.name", { ns: "order" }),
          amount: {
            currency_code: "EUR",
            value: payment.getBruttoPrice(),
            breakdown: {
              item_total: {
                currency_code: "EUR",
                value: payment.getNettoPrice(),
              },
              tax_total: {
                currency_code: "EUR",
                value: payment.getVatPrice(),
              },
            },
          },
          items: [
            {
              name: t("product.itemname", {
                ns: "order",
                count: payment_reference.amount,
              }),
              quantity: 1,
              unit_amount: {
                currency_code: "EUR",
                value: payment.getNettoPrice(),
              },
              tax: {
                currency_code: "EUR",
                value: payment.getVatPrice(),
              },
              category: "DIGITAL_GOODS",
              description: t("product.description", { ns: "order" }),
            },
          ],
          soft_descriptor: payment_reference.public_id,
        },
      ],
      application_context: {
        brand_name: "SUMA-EV",
        shipping_preference: "NO_SHIPPING",
      },
    };

    if (creditcard_payment) {
      paypal_order["payment_source"] = {
        card: {
          attributes: {
            verification: {
              method: "SCA_WHEN_REQUIRED",
            },
          },
          experience_context: {
            shipping_preference: "NO_SHIPPING",
          },
        },
      };
    }

    return this.#generateAccessToken().then((access_token) => {
      return fetch(`${this.#base}/v2/checkout/orders`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${access_token}`,
        },
        body: JSON.stringify(paypal_order),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.status !== "CREATED") {
            console.error(data);
            throw "Couldn't create Payment";
          }
          this.#order_id = data.id;
          this.#order_status = data.status;
          return this;
        });
    });
  }

  async captureOrder() {
    const accessToken = await this.#generateAccessToken();

    const url = `${base}/v2/checkout/orders/${this.#order_id}/capture`;

    return fetch(url, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((response) => response.json())
      .then((response_data) => {
        if (
          response_data.status !== "COMPLETED" ||
          response_data.purchase_units[0].payments.captures[0].status !==
            "COMPLETED"
        ) {
          throw response_data;
        }
        return response_data;
      });
  }

  async processWebhook(
    auth_algo,
    cert_url,
    transmission_id,
    transmission_sig,
    transmission_time,
    webhook_event // PayPal Webhook Event data
  ) {
    let webhook_id_promise = this.#getWebhookId();
    let access_token_promise = this.#generateAccessToken();
    let verification_url = `${base}/v1/notifications/verify-webhook-signature`;

    let webhook_data = await Promise.all([
      webhook_id_promise,
      access_token_promise,
    ])
      .then(([webhook_id, access_token]) =>
        fetch(verification_url, {
          method: "post",
          headers: {
            Authorization: `Bearer ${access_token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            auth_algo: auth_algo,
            cert_url: cert_url,
            transmission_id: transmission_id,
            transmission_sig: transmission_sig,
            transmission_time: transmission_time,
            webhook_event: webhook_event,
            webhook_id: webhook_id,
          }),
        })
      )
      .then((response) => {
        if (response.status !== 200) {
          throw "Received status code " + response.status + " from PayPal API.";
        } else {
          return response.json();
        }
      });

    if (
      !webhook_data.verification_status ||
      webhook_data.verification_status !== "SUCCESS"
    ) {
      throw "Webhook Verification was not successfull";
    } else {
      if (webhook_event.event_type === "PAYMENT.CAPTURE.COMPLETED") {
        // Check for a completed payment that did not get processed by us
        let payment_reference = webhook_event.resource.invoice_id;
        if (!payment_reference) {
          throw "No Order ID attached";
        }
        return PaymentReference.LOAD_FROM_PUBLIC_ID(payment_reference).then(
          (payment_reference) => {
            let capture = webhook_event.resource;
            if (capture.status !== "COMPLETED") {
              return Promise.reject("Payment not completed");
            }
            return payment_reference.createPayment({
              price: parseFloat(capture.amount.value),
              converted_price: parseFloat(capture.amount.value),
              converted_currency: capture.amount.currency_code,
              payment_processor: Paypal.NAME,
              payment_processor_id: capture.id,
              payment_processor_data: capture,
            });
          }
        );
      } else if (webhook_event.event_type === "PAYMENT.CAPTURE.REFUNDED") {
        // Check for a completed payment that did not get processed by us
        let payment_reference = webhook_event.resource.invoice_id;
        if (!payment_reference) {
          throw "No Order ID attached";
        }
        return PaymentReference.LOAD_FROM_PUBLIC_ID(payment_reference).then(
          (payment_reference) => {
            let capture = webhook_event.resource;
            if (capture.status !== "COMPLETED") {
              return Promise.reject("Payment not completed");
            }
            return payment_reference.createPayment({
              price: parseFloat(capture.amount.value) * -1,
              converted_price: parseFloat(capture.amount.value) * -1,
              converted_currency: capture.amount.currency_code,
              payment_processor: Paypal.NAME,
              payment_processor_id: capture.id,
              payment_processor_data: capture,
            });
          }
        );
      } else if (webhook_event.event_type === "CHECKOUT.ORDER.APPROVED") {
        // Check for a completed payment that did not get processed by us
        let payment_reference = webhook_event.resource.invoice_id;
        if (!payment_reference) {
          throw "No Order ID attached";
        }
        return PaymentReference.LOAD_FROM_PUBLIC_ID(payment_reference).then(
          (payment_reference) => {
            let order = webhook_event.resource;
            let payment_promises = [];
            for (let i = 0; i < order.purchase_units.length; i++) {
              let purchase_unit = order.purchase_units[i];
              for (let j = 0; j < purchase_unit.payments.captures.length; j++) {
                let capture = purchase_unit.payments.captures[j];
                if (
                  capture.status !== "COMPLETED" ||
                  capture.amount.currency_code !== "EUR"
                ) {
                  continue;
                }
                payment_promises.push(
                  payment_reference.createPayment({
                    price: parseFloat(capture.amount.value),
                    converted_price: parseFloat(capture.amount.value),
                    converted_currency: capture.amount.currency_code,
                    payment_reference_id: payment_reference.id,
                    payment_processor: Paypal.NAME,
                    payment_processor_id: capture.id,
                    payment_processor_data: capture,
                  })
                );
              }
            }
          }
        );
      } else {
        console.log(req.body);
        throw "Webhook not implemented";
      }
    }
  }

  /**
   *
   * @param {float} amount
   * @returns
   */
  async refundOrder(amount) {
    return this.get_order_details().then((order_data) => {
      let refund_urls = [];

      // Multiple Purchase units and payments might be associated with this order
      //
      order_data.purchase_units.forEach((purchase_unit, index) => {
        purchase_unit.payments.captures.forEach((payment, index) => {
          payment.links.forEach((link, index) => {
            if (link.rel === "refund") {
              refund_urls.push({
                refund_url: link.href,
                amount: parseFloat(payment.amount.value),
              });
              return false;
            }
          });
        });
      });

      return this.#generateAccessToken().then((access_token) => {
        let fetches = [];
        refund_urls.forEach((refund) => {
          let amount_to_refund = refund.amount;
          if (amount <= 0) {
            return;
          } else {
            if (amount_to_refund > amount) {
              amount_to_refund = amount;
              amount = 0;
              fetches.push(
                fetch(refund.refund_url, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${access_token}`,
                  },
                  body: JSON.stringify({
                    amount: {
                      value: amount_to_refund,
                      currency_code: "EUR",
                    },
                  }),
                })
              );
            } else {
              amount -= refund.amount;
              fetches.push(
                fetch(refund.refund_url, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${access_token}`,
                  },
                })
                  .then((response) => {
                    if (response.status !== 201) {
                      console.debug(response);
                      throw "Error issuing refund.";
                    } else {
                      return response.json();
                    }
                  })
                  .then((response_json) => {
                    if (response_json.status !== "COMPLETED") {
                      console.debug(response_json);
                      throw "Error issuing refund.";
                    } else {
                      return true;
                    }
                  })
              );
            }
          }
        });
        return Promise.all(fetches);
      });
    });
  }

  async verify_3D() {
    return this.get_order_details().then((order_details) => {
      if (
        order_details.payment_source.card.hasOwnProperty(
          "authentication_result"
        )
      ) {
        let authentication_result =
          order_details.payment_source.card.authentication_result;
        let liability_shift = authentication_result.liability_shift;
        let authentication_status = null;
        if (
          authentication_result.three_d_secure.hasOwnProperty(
            "authentication_status"
          )
        ) {
          authentication_status =
            authentication_result.three_d_secure.authentication_status;
        }
        let enrollment_status =
          authentication_result.three_d_secure.enrollment_status;
        if (enrollment_status == "Y") {
          switch (authentication_status) {
            case "Y":
              if (["POSSIBLE", "YES"].includes(liability_shift)) {
                return true;
              } else {
                return Promise.reject(order_details);
              }
            case "N":
              if (liability_shift == "NO") {
                return Promise.reject(order_details);
              } else {
                return true;
              }
            case "R":
              if (liability_shift == "NO") {
                return Promise.reject(order_details);
              } else {
                return true;
              }
            case "A":
              if (liability_shift == "POSSIBLE") {
                return true;
              } else {
                return Promise.reject(order_details);
              }
            case "U":
              if (["UNKNOWN", "NO"].includes(liability_shift)) {
                return Promise.reject(order_details);
              } else {
                return true;
              }
            case "C":
              if (liability_shift == "UNKNOWN") {
                return Promise.reject(order_details);
              } else {
                return true;
              }
            default:
              return Promise.reject(order_details);
          }
        } else if (enrollment_status == "N") {
          if (liability_shift == "NO") {
            return true;
          } else {
            return Promise.reject(order_details);
          }
        } else if (enrollment_status == "U") {
          switch (liability_shift) {
            case "NO":
              return true;
            case "UNKNOWN":
              return Promise.reject(order_details);
            default:
              return Promise.reject(order_details);
          }
        } else if (enrollment_status == "B") {
          if (liability_shift == "NO") {
            return true;
          } else {
            return Promise.reject(order_details);
          }
        } else {
          return Promise.reject(order_details);
        }
      } else {
        return true;
      }
    });
  }

  async get_order_details() {
    let url = `${base}/v2/checkout/orders/${this.#order_id}`;
    const access_token = await this.#generateAccessToken();
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    });
    if (response.status == 200) {
      return response.json();
    } else {
      throw "Couldn't retrieve Order";
    }
  }

  async #generateAccessToken() {
    const auth = Buffer.from(this.#client_id + ":" + this.#app_secret).toString(
      "base64"
    );

    return fetch(`${this.#base}/v1/oauth2/token`, {
      method: "post",
      body: "grant_type=client_credentials",
      headers: {
        Authorization: `Basic ${auth}`,
      },
    })
      .then((response) => response.json())
      .then((response_json) => {
        return response_json.access_token;
      });
  }

  async verifyWebhook() {
    let domain = config.get("app.url").replace(/\/+$/, "");

    if (!domain.startsWith("https://")) {
      // Do not attempt to register a webhook for localhost etc.
      return;
    }

    if (await __redis_client.get(webhook_redis_key)) {
      return;
    }
    const accessToken = await this.#generateAccessToken();
    return fetch(`${base}/v1/notifications/webhooks`, {
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((response) => response.json())
      .then((webhooks) => {
        let webhook_id = undefined;
        webhooks.webhooks.forEach(async (webhook) => {
          if (webhook.url.startsWith(domain)) {
            webhook_id = webhook.id;
          } else if (webhook.url.match(/^https:\/\/.*\.ngrok\.io/)) {
            // Another ngrok development webhook. Delete it.
            let promises = [];
            webhook.links.forEach((link) => {
              if (link.rel === "delete") {
                promises.push(
                  fetch(link.href, {
                    method: link.method,
                    headers: {
                      Authorization: `Bearer ${accessToken}`,
                    },
                  })
                );
              }
            });
            await Promise.all(promises);
          }
        });
        if (webhook_id !== undefined) {
          return __redis_client
            .setex(webhook_redis_key, 3600, webhook_id)
            .then(async () => {
              throw "WEBHOOK_ALREADY_REGISTERED"; // Webhook already registered
            });
        } else {
          // Webhook does not yet exist
          console.log("Webhook does not yet exist");
          return true;
        }
      })
      .then(async () => {
        // Webhook does not exist yet
        return fetch(`${base}/v1/notifications/webhooks`, {
          method: "post",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          body: JSON.stringify({
            event_types: [
              { name: "PAYMENT.CAPTURE.COMPLETED" },
              { name: "PAYMENT.CAPTURE.REFUNDED" },
              { name: "PAYMENT.CAPTURE.REVERSED" },
              { name: "CHECKOUT.ORDER.APPROVED" },
            ],
            url: domain + "/webhooks/paypal/webhook",
          }),
        })
          .then((response) => {
            if (response.status !== 201) {
              return Promise.reject(response);
            } else {
              return response.json();
            }
          })
          .then((response_data) => {
            console.log(JSON.stringify(response_data));
            __redis_client.setex(webhook_redis_key, 3600, response_data.id);
          });
      })
      .catch((reason) => {
        if (reason !== "WEBHOOK_ALREADY_REGISTERED") {
          console.error("Could not register Webhook for PayPal.");
          console.error(reason);
        }
      });
  }

  async #getWebhookId() {
    let webhook_id = await __redis_client.get(webhook_redis_key);
    return webhook_id;
  }

  // Client Token for handling credit card payments
  async generateClientToken() {
    const accessToken = await this.#generateAccessToken();

    const response = await fetch(`${base}/v1/identity/generate-token`, {
      method: "post",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Accept-Language": "en_US",
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    return data.client_token;
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {Object}
   */
  serialize() {
    return {
      processor_name: "Paypal",
      order_id: this.#order_id,
      order_status: this.#order_status,
    };
  }

  static DESERIALIZE(data) {
    return new Paypal(data.order_id, data.order_status);
  }

  /**
   *
   * Javascripts calculations with floating points produce weird artifacts. This method is to fix that issue
   *
   * @param {Float} value
   * @param {Int} precision
   * @returns float
   */
  #fixRounding(value, precision) {
    var power = Math.pow(10, precision || 0);
    return Math.round(value * power) / power;
  }
}

module.exports = Paypal;
