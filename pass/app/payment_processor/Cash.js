const PaymentProcessor = require("./PaymentProcessor");
const i18next = require("i18next");

class Cash extends PaymentProcessor {
  static get NAME() {
    return "Cash";
  }
  /**
   *
   * Constructs a new cash Payment
   *
   */
  constructor() {
    super();
  }

  /**
   *
   * @param {PaymentReference} payment_reference
   * @param {i18next.TFunction} t
   */
  async createOrder(payment_reference, t) {
    return Promise.resolve();
  }

  async captureOrder() {
    return Promise.resolve();
  }
  /**
   *
   * @returns {boolean}
   */
  isRefundSupported() {
    return false;
  }
  async refundOrder() {
    throw new Error("Cash Payments cannot be refunded");
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {Object}
   */
  serialize() {
    return {
      processor_name: "Cash",
    };
  }

  /**
   * Turns class object into serializable data to store in redis db
   *
   * @returns {PaymentProcessor}
   */
  static DESERIALIZE(data) {
    return new Cash();
  }
}

module.exports = Cash;
