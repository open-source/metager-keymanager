const Knex = require("knex");
const Redis = require("ioredis");
const config = require("config");
const Key = require("./Key");

const dayjs = require("dayjs");
const Payment = require("./Payment");
const Zammad = require("./Zammad");

/**
 * @typedef {Object} PaymentReferenceDB
 * @property {number} PaymentReferenceDB.id
 * @property {number} PaymentReferenceDB.amount
 * @property {number} PaymentReferenceDB.price
 * @property {string} PaymentReferenceDB.key
 * @property {string} PaymentReferenceDB.created_at
 * @property {string} PaymentReferenceDB.expires_at
 */
/** @returns {Knex.QueryBuilder<PaymentReferenceDB, {}>} */
const PaymentReferences = () => __database_client("payment_references");

/**
 * Represents a Request to purchase tokens.
 * Includes information about the price etc
 *
 * @property {Knex} DB_CLIENT
 */
class PaymentReference {
  static get TABLENAME() {
    return "payment_references";
  }
  static get DEFAULT_EXPIRATION_HOURS() {
    return 6;
  }

  /**
   *
   * @param {Object} data
   * @param {number} data.id
   * @param {number} data.amount
   * @param {number} data.price
   * @param {Key} data.key
   * @param {string} data.created_at
   * @param {string} data.expires_at
   */
  constructor(data) {
    this.id = data.id;
    this.public_id =
      "Z" + (data.id + config.get("price.number_range.payment_reference"));
    this.amount = data.amount;
    this.price = data.price;
    this.key = data.key;
    this.created_at = dayjs(data.created_at);
    this.expires_at = dayjs(data.expires_at);
  }

  /**
   * Creates a new Token Request
   *
   * @param {number} amount
   * @param {string} key
   * @param {dayjs.Dayjs} expiration
   * @param {boolean} expiration
   * @returns
   */
  static async CREATE_NEW_REQUEST(
    amount,
    key,
    expiration = dayjs().add(PaymentReference.DEFAULT_EXPIRATION_HOURS, "hours"),
    ignore_charge_limit = false
  ) {
    // Calculate price from amount
    let price = amount * config.get("price.per_token");
    price = price.toFixed(2);

    // Validate that Key can be charged with another order
    /**
     * @type {Key}
     */
    let loaded_key = null;

    return Key.GET_KEY(key)
      .then((key) => {
        if (!key.isChargable() && !ignore_charge_limit) {
          throw "Key cannot be charged";
        } else {
          loaded_key = key;
          return key;
        }
      })
      .then((key) =>
        PaymentReferences().insert(
          {
            amount: amount,
            price: price,
            key: key.get_key(),
            created_at: dayjs().format("YYYY-MM-DD HH:mm:ss"),
            expires_at: expiration.format("YYYY-MM-DD HH:mm:ss"),
          },
          "*"
        )
      )
      .then((token_data) => {
        if (token_data.length === 0) {
          return Promise.reject("Error creating PaymentReference");
        }
        return Key.GET_KEY(token_data[0].key, false).then((key) => {
          token_data[0].key = key;
          return new PaymentReference(token_data[0]);
        });
      });
  }

  /**
   * Loads a PaymentReference object from ID
   *
   * @param {number} public_id
   *
   * @returns {Promise<PaymentReference>}
   */
  static async LOAD_FROM_PUBLIC_ID(public_id, key = null) {
    let matcher = public_id.match(/^(Z)?(\d+)$/);
    if (!matcher) {
      return Promise.reject("Invalid PaymentReference ID");
    }
    public_id =
      parseInt(matcher[2]) - config.get("price.number_range.payment_reference");

    if (key === null) {
      return PaymentReferences()
        .where("id", public_id)
        .first()
        .then((token_data) => {
          if (token_data === undefined) {
            return Promise.reject("Cannot find Token Request");
          }
          return Key.GET_KEY(token_data.key, false).then((key) => {
            token_data.key = key;
            return new PaymentReference(token_data);
          });
        });
    } else {
      return PaymentReferences()
        .where("id", public_id)
        .andWhere("key", key)
        .first()
        .then((token_data) => {
          if (token_data === undefined) {
            return Promise.reject("Cannot find Token Request");
          }
          return Key.GET_KEY(token_data.key, false).then((key) => {
            token_data.key = key;
            return new PaymentReference(token_data);
          });
        });
    }
  }

  static async LOAD_FROM_ID(id, key = null) {
    if (key === null) {
      return PaymentReferences()
        .where("id", id)
        .first()
        .then((token_data) => {
          if (token_data === undefined) {
            return Promise.reject("Cannot find Token Request");
          }
          return Key.GET_KEY(token_data.key, false).then((key) => {
            token_data.key = key;
            return new PaymentReference(token_data);
          });
        });
    } else {
      return PaymentReferences()
        .where("id", id)
        .andWhere("key", key)
        .first()
        .then((token_data) => {
          if (token_data === undefined) {
            return Promise.reject("Cannot find Token Request");
          }
          return Key.GET_KEY(token_data.key, false).then((key) => {
            token_data.key = key;
            return new PaymentReference(token_data);
          });
        });
    }
  }

  /**
   *
   * @param {string} public_id
   * @returns {Promise<number>|Promise<never>}
   */
  static async CONVERT_TO_INTERNAL_ID(public_id) {
    return new Promise((resolve, reject) => {
      let matcher = public_id.match(/^(Z)?(\d+)$/);
      if (!matcher) {
        reject("Invalid public id");
      }
      resolve(
        parseInt(matcher[2]) -
        config.get("price.number_range.payment_reference")
      );
    });
  }

  static async LOAD_REFERENCES_FOR_KEY(key) {
    let payment_references = [];
    return PaymentReferences()
      .where("key", key.get_key())
      .orderBy("created_at", "desc")
      .then(references => {
        for (let i = 0; i < references.length; i++) {
          payment_references.push(new PaymentReference(references[i]));
        }
        return payment_references;
        let test = "test";
      });
  }

  /**
   * Gets the Key associated with this request
   *
   * @param {boolean} writable Should the key be opened writable
   *
   * @returns {Promise<Key>}
   */
  async getKey(writable) {
    return PaymentReferences()
      .select("key")
      .where("id", this.id)
      .first()
      .then((payment_reference) =>
        Key.GET_KEY(payment_reference.key, writable)
      );
  }

  async delete(key = undefined) {
    let query = PaymentReferences()
      .del()
      .where("payment_references.id", this.id);
    if (key) {
      query = query.where("payment_references.key", key);
    }
    query = query.whereNotIn("id", function () {
      this.select("payment_reference_id").from("payments");
    });
    return query.then((result) => {
      return result;
    });
  }

  /**
   * Checks if PaymentReference exists
   *
   * @returns {Promise<true>|Promise<never>}
   */
  async exists() {
    return PaymentReferences()
      .where("id", this.id)
      .then((result) => {
        if (result.length !== 1) {
          return Promise.reject("PaymentReference does not exist");
        } else {
          return true;
        }
      });
  }

  /**
   *
   * @param {dayjs.Dayjs} expiration
   * @returns {Promise<PaymentReference>|Promise<never>}
   */
  async setExpiration(expiration) {
    return PaymentReferences()
      .where("id", this.id)
      .update(
        {
          expires_at: expiration.format("YYYY-MM-DD HH:mm:ss"),
        },
        "*"
      )
      .then((token_data) => {
        this.expires_at = dayjs(token_data.expires_at);
        return new PaymentReference(token_data);
      });
  }

  /**
   * Creates a new Payment for this PaymentReference
   * Attention: Tokens will be charged/discharged to the key
   *
   * @param {Object} data
   * @param {number} data.price
   * @param {number} data.converted_price
   * @param {string} data.converted_currency
   * @param {string} data.payment_processor
   * @param {string} [data.payment_processor_id]
   * @param {Object} [data.payment_processor_data]
   *
   * @returns {Promise<Payment>}
   */
  async createPayment(data) {
    data.payment_reference_id = this.id;
    return Payment.CREATE_NEW_PAYMENT(data).then((payment) => {
      return this.getKey(true).then(async (key) => {
        await this.setExpiration(dayjs().add(10, "year"));
        let charge_amount = Math.round(
          (payment.price / this.price) * this.amount
        );
        if (charge_amount > 0) {
          key.charge_key_order(
            charge_amount,
            this.id,
            dayjs().add(config.get("keys.expiration_years"), "year")
          );
        } else {
          key.discharge_key(
            Math.abs(charge_amount),
            this.id,
            dayjs().add(config.get("keys.expiration_years"), "year")
          );
        }
        return Zammad.CREATE_NOTIFICATION(charge_amount, payment.price, payment.payment_processor.serialize().processor_name)
          .then(() => key.save())
          .then(key => payment);
      });
    });
  }

  /**
   * Charges the key without creating a Payment (Manual Charge)
   *
   * @returns
   */
  async chargeKey() {
    return this.getKey(true).then((key) => {
      let expiration = dayjs().add(config.get("keys.expiration_years"), "year");
      return this.setExpiration(expiration).then(() => {
        key.charge_key_order(this.amount, this.id, expiration);
        return Zammad.CREATE_NOTIFICATION(this.amount, 0, "Manual").then(() => key.save());
      });
    });
  }
}

module.exports = PaymentReference;
