const paypal_client = require("@paypal/paypal-js");

initialize_paypal_payments();

function initialize_paypal_payments() {
  let script_data = {
    "client-id": document.querySelector("input[name=paypal-client-id]").value,
    components: ["buttons", "marks", "payment-fields", "funding-eligibility"],
    "disable-funding": "sofort,ideal",
    currency: "EUR",
  };

  let client_token_field = document.querySelector(
    "input[name=paypal-client-token]"
  );
  if (client_token_field) {
    script_data["data-client-token"] = client_token_field.value;
  }

  let funding_source = document.querySelector(
    "input[name=paypal-funding-source]"
  ).value;

  if (funding_source !== "paypal") {
    script_data["enable-funding"] = funding_source;
  }

  let checkout_data = get_paypal_checkout_data(funding_source);

  if (funding_source == "card") {
    script_data.components = ["card-fields", "funding-eligibility", "buttons"];
  }

  paypal_client
    .loadScript(script_data)
    .then((paypal) => {
      if (!paypal.isFundingEligible(funding_source)) {
        // Funding with this source is not available to the user
        let disabled_funding = localStorage.getItem("funding_not_eligible");
        if (disabled_funding === null) {
          disabled_funding = [];
        } else {
          disabled_funding = JSON.parse(disabled_funding);
        }
        disabled_funding.push(funding_source);
        localStorage.setItem(
          "funding_not_eligible",
          JSON.stringify(disabled_funding)
        );
        document.location.href = document.querySelector(
          "input[name=funding-source-not-eligible-url]"
        ).value;
        return;
      }
      if (funding_source === "card") {
        loadCardPayment(checkout_data);
      } else if (funding_source === "paypal") {
        paypal
          .Buttons(get_paypal_checkout_data(null))
          .render("#paypal-payment-button");
      } else {
        let payment_fields_container = document.querySelector(
          "#paypal-payment-fields"
        );
        if (payment_fields_container) {
          payment_fields_container.style.display = "block";
        }
        paypal
          .PaymentFields({
            fundingSource: funding_source,
            style: {},
            fields: {},
          })
          .render("#paypal-payment-fields");
        let button_data = get_paypal_checkout_data(funding_source);
        paypal.Buttons(button_data).render("#paypal-payment-button");
      }
    })
    .catch((err) => {
      // ToDo Handle error
      console.error("failed to load the PayPal JS SDK script", err);
    });
}

function get_paypal_checkout_data(funding_source) {
  let button_style = getComputedStyle(document.body).getPropertyValue(
    "--paypal-button-style"
  );
  let checkout_data = {
    style: {
      color: button_style,
      height: 50,
    },
    fundingSource: funding_source,
    onClick: () => {
      validateRevocation();
    },
    createOrder: () => {
      let checkout_paypal_create_order_url =
        document.querySelector(
          "#paypal-checkout input[name=paypal-order-base-url]"
        ).value + "/create";

      return fetch(checkout_paypal_create_order_url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
      })
        .then((response) => response.json())
        .then((order) => {
          document.getElementById(
            "paypal-checkout"
          ).dataset.payment_reference_id = order.payment_reference;
          return order.paypal_order_id;
        });
    },
    onCancel: (data, actions) =>
      cancelPayment(
        document.getElementById("paypal-checkout").dataset.payment_reference_id
      ),
    onError: (err) => {
      console.error(err);
      return cancelPayment(
        document.getElementById("paypal-checkout").dataset.payment_reference_id
      );
    },
    onApprove: () => {
      let checkout_paypal_capture_order_url =
        document.querySelector(
          "#paypal-checkout input[name=paypal-order-base-url]"
        ).value + "/capture";
      return fetch(checkout_paypal_capture_order_url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify({
          payment_reference:
            document.getElementById("paypal-checkout").dataset
              .payment_reference_id,
        }),
      })
        .then((response) => {
          if (response.status !== 200) {
            return response.json().then((json_response) => {
              throw json_response;
            });
          } else {
            return response.json();
          }
        })
        .then((orderData) => {
          if (typeof orderData.redirect_url !== "undefined") {
            document.location.href = orderData.redirect_url;
          }
        });
    },
    onInit: (data, actions) => {
      actions.disable();
      let revocation_checkbox = document.getElementById("revocation");
      revocation_checkbox.addEventListener("change", (e) => {
        if (e.target.checked) {
          actions.enable();
        } else {
          actions.disable();
        }
      });
      document
        .getElementById("loading_paypal_funding_source")
        .classList.add("hidden");
      document
        .getElementById("paypal-payment-fields")
        .classList.remove("hidden");
      document
        .getElementById("paypal-payment-button")
        .classList.remove("hidden");
      document
        .getElementById("revocation-container")
        .classList.remove("hidden");
    },
  };
  if (funding_source) {
    checkout_data.fundingSource = funding_source;
  }
  if (funding_source == "card") {
    delete checkout_data.onCancel;
  }
  return checkout_data;
}

function cancelPayment(payment_reference) {
  let checkout_paypal_cancel_order_url =
    document.querySelector("#paypal-checkout input[name=paypal-order-base-url]")
      .value + "/cancel";
  return fetch(checkout_paypal_cancel_order_url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify({
      payment_reference: payment_reference,
    }),
  });
}

function loadCardPayment(checkout_data) {
  let container = document.getElementById("content-container");
  let card_form = document.getElementById("card-form-skeleton").cloneNode(true);
  card_form.id = "card-form";
  card_form.classList.remove("hidden");
  document.getElementById("card-form-skeleton").remove();
  container.appendChild(card_form);

  // Initialize Card Fields
  let cardFields_options = get_paypal_checkout_data("card");

  let background_color =
    window
      .getComputedStyle(document.querySelector(":root"))
      .getPropertyValue("--background-color") ?? "white";
  let text_color =
    window
      .getComputedStyle(document.querySelector(":root"))
      .getPropertyValue("--font-color") ?? "black";

  cardFields_options.style = {
    body: {
      padding: 0,
    },
    input: {
      background: background_color,
      color: text_color,
      "font-size": "16px",
      padding: "0.4rem 0.75rem",
    },
  };
  let cardFields = paypal.CardFields(cardFields_options);
  document
    .getElementById("loading_paypal_funding_source")
    .classList.add("hidden");
  if (!cardFields.isEligible()) {
    showError("generic");
    return;
  }

  const nameField = cardFields.NameField({ placeholder: "John Doe" });
  nameField.render("#card-name");

  const numberField = cardFields.NumberField({
    placeholder: "4111 1111 1111 1111",
  });
  numberField.render("#card-number");

  const expiryField = cardFields.ExpiryField({ placeholder: "123" });
  expiryField.render("#card-expiration");

  const cvvField = cardFields.CVVField();
  cvvField.render("#card-cvv");

  card_form.addEventListener("submit", (event) => {
    if (!card_form.checkValidity()) {
      return;
    }
    event.preventDefault();
    hideErrors();
    lockForm(true);
    cardFields
      .getState()
      .then((data) => {
        // Submit only if the current
        // state of the form is valid
        if (!data.isFormValid) {
          showError(`error-1330`);
        } else {
          return cardFields
            .submit()
            .then(() => {})
            .catch((error) => {
              console.error(error);

              let error_code_shown = false;
              try {
                let processor_response_code =
                  error.purchase_units[0].payments.captures[0]
                    .processor_response.response_code;
                showError(`error-${processor_response_code}`);
                error_code_shown = true;
              } catch (e) {}

              try {
                let card_errors_container =
                  document.querySelector("#card-errors");
                if (card_errors_container.classList.contains("hidden")) {
                  card_errors_container.classList.remove("hidden");
                }
                for (let i = 0; i < error.details.length; i++) {
                  let error_container = document.createElement("div");
                  error_container.classList.add("error");
                  error_container.textContent = error.details[i].description;
                  card_errors_container.appendChild(error_container);
                }
                error_code_shown = true;
              } catch (e) {}
              if (!error_code_shown) {
                if (error.toString != undefined) {
                  error = error.toString();
                }
                if (error.includes != undefined && error.includes("3DS")) {
                  showError("error-3ds");
                } else {
                  showError(`error-1330`);
                }
              }
            });
        }
      })
      .finally(() => {
        lockForm(false);
      });
  });
  return;
  // Show the Form
  document
    .getElementById("loading_paypal_funding_source")
    .classList.add("hidden");
  document.getElementById("paypal-payment-card").classList.remove("hidden");
  document.getElementById("revocation").addEventListener("invalid", (e) => {
    document.querySelector("div.revocation-required-error").style.display =
      "block";
  });
  paypal.HostedFields.render({
    styles: {
      input: {
        padding: "0 .5rem",
        "font-family": "Liberation Sans",
      },
    },
    fields: {
      number: {
        selector: "#card-number",
        placeholder: "4111 1111 1111 1111",
      },
      expirationDate: {
        selector: "#expiration-date",
        placeholder: "MM/YY",
      },
      cvv: {
        selector: "#cvv",
        placeholder: "123",
      },
    },
    createOrder: checkout_data.createOrder,
  }).then((cardFields) => {
    document
      .getElementById("paypal-card-form")
      .addEventListener("submit", (e) => {
        e.preventDefault();
        document.querySelector("div.revocation-required-error").style.display =
          "none";
        // Hide all errors
        document
          .querySelectorAll("#paypal-card-errors > p")
          .forEach((element) => {
            element.classList.add("hidden");
          });
        document
          .querySelectorAll("#paypal-card-form label")
          .forEach((element) => {
            element.classList.remove("error");
          });
        // Disable Button
        let submit_button = document.getElementById("submit-credit-card");
        submit_button.disabled = true;
        submit_button.classList.add("loading");

        let cardfield_data = {
          contingencies: ["SCA_ALWAYS"],
        };
        if (document.getElementById("card-holder-name")) {
          cardfield_data.cardholderName =
            document.getElementById("card-holder-name").value;
        }

        cardFields
          .submit(cardfield_data)
          .then((payload) => {
            checkout_data
              .onApprove()
              .then(() => {
                // Enable Button
                submit_button.disabled = undefined;
                submit_button.classList.remove("loading");
              })
              .catch((reason) => {
                let payment_failed = false;
                reason.errors.forEach((error) => {
                  if (error.type && error.type === "PAYPAL_CARD_3D_ERROR") {
                    payment_failed = true;
                    document
                      .getElementById("paypal-card-errors-3d")
                      .classList.remove("hidden");
                    cancelPayment(
                      document.getElementById("paypal-checkout").dataset
                        .payment_reference_id
                    );
                  } else if (
                    error.type &&
                    error.type === "PAYMENT_NOT_COMPLETED_ERROR"
                  ) {
                    payment_failed = true;
                    document
                      .getElementById("paypal-card-errors-generic")
                      .classList.remove("hidden");
                    cancelPayment(
                      document.getElementById("paypal-checkout").dataset
                        .payment_reference_id
                    );
                  } else {
                    // Capture was not yet possible ToDo redirect user to Order
                    // There is a chance that webhooks will capture the payment
                    console.error(reason);
                    console.log("Fehlgeschlagen");
                  }
                });
              });
          })
          .catch((reason) => {
            reason.details.forEach((detail) => {
              cancelPayment(
                document.getElementById("paypal-checkout").dataset
                  .payment_reference_id
              );
              if (detail.issue === "CARD_TYPE_NOT_SUPPORTED") {
                document
                  .getElementById("paypal-card-errors-invalid-card")
                  .classList.remove("hidden");
              } else if (detail.issue === "CARD_EXPIRED") {
                document
                  .getElementById("paypal-card-errors-expired")
                  .classList.remove("hidden");
              } else if (detail.field === "/payment_source/card/expiry") {
                document
                  .querySelector("#paypal-card-form label[for=expiration-date]")
                  .classList.add("error");
              } else if (detail.field === "/payment_source/card/number") {
                document
                  .querySelector("#paypal-card-form label[for=card-number]")
                  .classList.add("error");
              }
              // Enable Button
              submit_button.disabled = undefined;
              submit_button.classList.remove("loading");
            });
          });
      });
  });
}

function validateRevocation() {
  let revocation_checkbox = document.getElementById("revocation");
  if (!revocation_checkbox.checkValidity()) {
    if (!revocation_checkbox.reportValidity()) {
      document.querySelector("div.revocation-required-error").style.display =
        "block";
    }
    return;
  } else {
    document.querySelector("div.revocation-required-error").style.display =
      "none";
  }
}

function showError(errorId) {
  let error_container = document.querySelector("#card-errors");
  if (!error_container) {
    return;
  }
  if (error_container.classList.contains("hidden")) {
    error_container.classList.remove("hidden");
  }
  let error_element = error_container.querySelector(`#${errorId}`);
  if (!error_element) {
    error_element = error_container.querySelector("#error-generic");
  }
  if (error_element.classList.contains("hidden")) {
    error_element.classList.remove("hidden");
  }
}

function hideErrors() {
  let error_container = document.querySelector("#card-errors");
  if (!error_container) {
    return;
  }
  if (!error_container.classList.contains("hidden")) {
    error_container.classList.add("hidden");
  }
  error_container.querySelectorAll(".error").forEach((error_element) => {
    if (!error_element.classList.contains("hidden")) {
      error_element.classList.add("hidden");
    }
  });
}

/**
 * Prevents multiple submissions of the card form
 *
 * @param {boolean} lock
 */
function lockForm(lock) {
  let submit_button = document.querySelector("#card-form #card-submit");
  if (!submit_button) {
    return;
  }
  submit_button.disabled = lock;
}

module.exports = initialize_paypal_payments;
