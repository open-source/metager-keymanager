let open_chat_button = document.querySelector("button.open-chat");

let chat_id = open_chat_button.dataset.chatid;
let title = open_chat_button.dataset.title;
let chat = new ZammadChat({
    background: '#17608b',
    fontSize: '12px',
    title: title,
    chatId: chat_id,
    show: false,
    buttonClass: "open-chat",
    host: "wss://support.metager.de/ws"
});

// Show chat button when loaded

open_chat_button.classList.remove("hidden");

console.log(chat);
console.log(chat.state);