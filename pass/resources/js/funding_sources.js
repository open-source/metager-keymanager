let funding_sources = document.querySelectorAll("#payment .funding_source");

let disabled_sources = localStorage.getItem("funding_not_eligible");
if (disabled_sources !== null) {
  disabled_sources = JSON.parse(disabled_sources);
  disabled_sources.forEach((source) => {
    let source_element = document.querySelector("#payment #" + source);
    if (source_element) {
      source_element.classList.add("hidden");
    }
  });
}
