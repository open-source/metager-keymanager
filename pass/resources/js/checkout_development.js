const paypal_client = require("@paypal/paypal-js");

function execute_payment_paypal(encrypted_sales_receipts) {
  let payment_method_buttons = document.querySelectorAll(
    "#payment-providers > ul > li"
  );
  for (let i = 0; i < payment_method_buttons.length; i++) {
    payment_method_buttons[i].dataset.active = false;
  }

  let development_payment_option_button = document.getElementById(
    "payment_method_development"
  );
  let payment_container = document.getElementById("payment-information");
  payment_container.textContent = "";

  development_payment_option_button.dataset.active = true;

  let execute_payment_button = document.createElement("button");
  execute_payment_button.textContent = "Zahlung simulieren";
  payment_container.appendChild(execute_payment_button);

  execute_payment_button.addEventListener("click", () => {
    fetch("/checkout/payment/order/development", {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8",
      },
      body: JSON.stringify({
        order_id: document.querySelector("input[name=order_id]").value,
        expires_at: document.querySelector("input[name=expires_at]").value,
        amount: document.querySelector("input[name=amount]").value,
        unit_size: document.querySelector("input[name=unit_size]").value,
        price_per_unit: document.querySelector("input[name=price_per_unit]")
          .value,
        public_key_n: document.querySelector("input[name=public_key_n]").value,
        public_key_e: document.querySelector("input[name=public_key_e]").value,
        integrity: document.querySelector("input[name=integrity]").value,
        encrypted_sales_receipts: encrypted_sales_receipts,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("There was an error executing the payment");
        }
        return response.json();
      })
      .then((data) => {
        let paymentEvent = new CustomEvent("payment-complete", {
          detail: {
            order_id: data.order_id,
            expires_at: data.expires_at,
            signatures: data.signatures,
          },
          bubbles: true,
          cancelable: true,
          composed: false,
        });
        development_payment_option_button.dispatchEvent(paymentEvent);
      });
  });
}

module.exports = execute_payment_paypal;
