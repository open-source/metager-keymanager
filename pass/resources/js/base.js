document.querySelectorAll(".share").forEach((element) => {
  if (navigator.share) {
    element.classList.add("active");
  }
  element.addEventListener("click", (e) => {
    let share_data = {
      url: document.getElementById(element.dataset.share_url_target).value,
      title: element.dataset.share_title,
    };
    navigator.share(share_data).catch((reason) => {
      console.error(reason);
    });
  });
});

document.querySelectorAll(".copy").forEach((element) => {
  element.classList.add("active");
  element.addEventListener("click", event => {
    event.preventDefault();
    let target = document.getElementById(element.dataset.target);
    target.focus();
    target.setSelectionRange(0, target.value.length);
    navigator.clipboard.writeText(target.value).then(() => {
      element.classList.add("success");
      setTimeout(() => {
        element.classList.remove("success");
      }, 1000);
    });
  });
});

document.querySelectorAll(".js-only").forEach((element) => {
  element.classList.remove("js-only");
});

document.querySelectorAll("#key-remove").forEach((element) => {
  element.addEventListener("click", e => {
    if (document.getElementById("plugin-btn") != null) return; // There is no extension or android app active
    e.preventDefault(); // Prevent direct loading of page
    let url = new URL(e.target.href);
    sendMessage({ type: "settings_remove", setting_key: "key" }).then(() => {
      document.location.href = url;
    })
  });
})

async function sendMessage(payload) {
  let message_id = (new Date()).getTime();
  let message = {
    sender: "webpage",
    message_id: message_id,
    payload: payload
  };

  return new Promise(resolve => {
    if (typeof mgapp == "undefined") {
      window.addEventListener("message", receiveMessage);
      window.postMessage(message);
    } else {
      mgapp.addEventListener("message", receiveMessage);
      mgapp.postMessage(JSON.stringify(message));
    }

    let timeout = setTimeout(() => {
      resolve(null);
    }, 10000);

    function receiveMessage(event) {
      if (typeof mgapp != "undefined" && typeof event.data == "string") {
        event.data = JSON.parse(event.data);
      }
      if ((event.source !== window && typeof mgapp == "undefined") || !["tokenmanager", "mgapp"].includes(event?.data?.sender) || event?.data?.message_id !== message_id) return;
      clearTimeout(timeout);
      window.removeEventListener("message", receiveMessage);
      resolve(event.data.payload);
    }
  });
}