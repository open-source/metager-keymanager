let scanner;
let container = document.getElementById("qr-scanner");

document.getElementById("qr-scan").addEventListener("pointerup", e => {
    document.querySelectorAll("#scan-qr-option .error").forEach(error => {
        error.style.display = "none";
    })
    let baseDir = document.getElementById("key-form").action.replace(/\/key\/enter$/, "");
    console.log(baseDir);
    import(`${baseDir}/js/qr-scanner.min.js`).then(async module => {
        const QrScanner = module.default;

        if (! await QrScanner.hasCamera()) {
            document.querySelector("#qr-camera-error").style.display = "block";
            return;
        }

        let video = document.querySelector("#qr-scanner > #scanner-container > video");
        let close_button = document.querySelector("#qr-scanner > #scanner-container a");
        container.style.display = "grid";

        scanner = new QrScanner(video, result => {
            scanner.stop();
            let url = result.data;
            try {
                url = new URL(url);
            } catch (error) {
                document.getElementById("qr-decode-error").style.display = "block";
                stopScanning();
            }
            let key = url.searchParams.get("key");
            if (key !== null) {
                location.href = `${baseDir}/key/` + encodeURIComponent(key);
            } else {
                document.getElementById("qr-decode-error").style.display = "block";
                stopScanning();
            }
        }, {
            highlightScanRegion: true,
            highlightCodeOutline: true
        });

        close_button.addEventListener("click", e => {
            e.preventDefault();
            stopScanning();
            return false;
        });

        scanner.start();
    });


});

function stopScanning() {
    scanner.stop();
    let highlights = document.querySelectorAll("#qr-scanner .scan-region-highlight");
    if (highlights) {
        highlights.forEach(element => {
            element.remove();
        });
    }
    scanner.stop();
    scanner.destroy();
    container.style.display = "none";
}

// Make Password field show key as cleartext
let key_input = document.getElementById("key");
key_input.addEventListener("focus", () => {
    key_input.type = "text";
});
key_input.addEventListener("blur", () => {
    key_input.type = "password";
});