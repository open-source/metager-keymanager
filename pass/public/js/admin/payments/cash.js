window.addEventListener("DOMContentLoaded", (event) => {
  updateConvertedPriceVisibility();
  document
    .querySelector("#converted_currency")
    .addEventListener("change", updateConvertedPriceVisibility);

  document
    .querySelector("#converted_price")
    .addEventListener("change", updateExchangeRateLink);
});

function updateConvertedPriceVisibility() {
  let select = document.querySelector("#converted_currency");
  let selectedOption = select.options[select.selectedIndex].value;

  let convertedPrice = document.querySelector("#price-group");
  if (selectedOption === "EUR") {
    convertedPrice.style.display = "none";
  } else {
    convertedPrice.style.display = "grid";
    updateExchangeRateLink();
  }
}

function updateExchangeRateLink() {
  let exchangeRateLink = document.querySelector(
    "#price-group > label > a"
  );
  let exchangeRateURL = new URL("https://www.xe.com/currencyconverter/convert");

  let currentAmount = document.querySelector("#converted_price").value;
  let currencySelect = document.querySelector("#converted_currency");
  let currentCurrency =
    currencySelect.options[currencySelect.selectedIndex].value;
  exchangeRateURL.searchParams.append("Amount", currentAmount);
  exchangeRateURL.searchParams.append("From", currentCurrency);
  exchangeRateURL.searchParams.append("To", "EUR");
  exchangeRateLink.href = exchangeRateURL.toString();
}
