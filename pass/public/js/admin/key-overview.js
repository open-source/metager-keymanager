let amount_input = document.querySelector("input[name=amount]");
let price_input = document.querySelector("input[name=price]");

amount_input.addEventListener("change", e => {
    if (e.target.value) {
        price_input.value = "";
    }
});

price_input.addEventListener("change", e => {
    if (e.target.value) {
        amount_input.value = "";
    }
});