const default_searches = 250;
const default_estimate_months = 1;

document.getElementById("amount").addEventListener("input", multiplierChanged);
multiplierChanged();

function multiplierChanged() {
  let price_for_250 =
    document.querySelector("form.offer.default").dataset.price_for_250;
  let multiplier = document.getElementById("amount").value;
  let searches_element = document.querySelector(
    "#offers > .offer.default > h1"
  );
  let estimate_element = document.querySelector(
    "#offers > .offer.default > p.hint > span"
  );
  let price_element = document.querySelector(
    "#offers > .offer.default > button.select"
  );
  searches_element.textContent = default_searches * multiplier;
  price_element.textContent = price_for_250 * multiplier + " €";
  estimate_element.textContent = default_estimate_months * multiplier;
}
