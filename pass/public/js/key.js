// Transfer Dialog

let transfer_dialog = document.getElementById("transfer-dialog");
let transfer_dialog_open_button = document.querySelector("#buttons > button.transfer");
let transfer_dialog_close_button = transfer_dialog.querySelector("div.close-group > button");
let transfer_dialog_codecontainer = transfer_dialog.querySelector("div.input-group > div");
let transfer_dialog_interval = null;
transfer_dialog_open_button.addEventListener("click", e => {
    openModal();
});
transfer_dialog_close_button.addEventListener("click", e => {
    closeModal();
})

function openModal() {
    transfer_dialog.showModal();
    checkCode();
    transfer_dialog_interval = setInterval(checkCode, 1000);
}

function checkCode() {
    getCode().then(code => {
        let current_code = transfer_dialog_codecontainer.textContent.trim();
        if (current_code != "Loading" && code != current_code) {
            console.log("Code invalidated closing");
            console.log(code, current_code);
            closeModal();
            return;
        } else {
            transfer_dialog_codecontainer.textContent = code;
        }
    })
}

function closeModal() {
    transfer_dialog.close();
    transfer_dialog_codecontainer.textContent = "Loading";
    if (transfer_dialog_interval != null) {
        clearInterval(transfer_dialog_interval);
        transfer_dialog_interval = null;
    }
}

async function getCode() {
    let url = transfer_dialog.querySelector("div.input-group").dataset.url;
    return fetch(url).then(res => res.json()).then(res => {
        return res.code;
    }).catch(reason => {
        console.error(reason);
        closeModal();
        return "Loading";
    })
}