let container = document.getElementById("create");
let keyContainer = document.getElementById("new_key");
let initializeButton = document.querySelector("div.initialize > button");
let copy_key_button = document.querySelector(".key.copy-group > button");
document.getElementById("key").remove();
let key_password_field = document.createElement("input");
key_password_field.type = "password";
key_password_field.id = "key";
key_password_field.name = "key";
document.getElementById("charge-form").appendChild(key_password_field);

let key = keyContainer.dataset.key;

let characters = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
];

container.dataset.state = "initialize";
keyContainer.parentNode
  .querySelector("button.button")
  .classList.add("disabled");
key_password_field.value = key;

let runtime = 2500; // Runtime in milliseconds
let starttime = Date.now();

initializeButton.addEventListener("click", (e) => {
  starttime = Date.now();
  container.dataset.state = "processing";
  generateRandomKey(0);
  let interval = setInterval(() => {
    if (starttime + runtime < Date.now()) {
      clearInterval(interval);
      container.dataset.state = "finished";
      keyContainer.parentNode
        .querySelector("button.button")
        .classList.remove("disabled");
      if (window.hasOwnProperty("history")) {
        history.replaceState(null, null, document.querySelector("#setting-url").value);
      }
      if (!tryToSetCookie()) {
        document
          .querySelector(".description-no-cookies")
          .classList.remove("hidden");
      }
    }
    let steps = runtime / 32;
    let skipped_characters = Math.ceil((Date.now() - starttime) / steps);
    generateRandomKey(skipped_characters);
  }, 100);
});

function generateRandomKey(skip) {
  let random_key = "";
  if (!skip) {
    skip = 0;
  } else {
    skip = Math.max(Math.min(32, skip), 0);
  }
  for (let i = 0; i < 32; i++) {
    let character = "";
    if (i == 8 || i == 12 || i == 16 || i == 20) {
      random_key += "-";
    }
    character = characters[Math.floor(Math.random() * characters.length)];
    if (skip > i) {
      character = key.replaceAll("-", "").charAt(i);
    }
    random_key += character;
  }
  keyContainer.value = random_key;
}

function tryToSetCookie() {
  let expires = new Date();
  expires.setFullYear(expires.getFullYear() + 5);
  document.cookie = `key=${key}; Max-Age=157680000; Path=/; Expires=${expires}; SameSite=Lax`;

  if (
    document.cookie.match(
      /key=[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+-[a-z0-9]+/
    )
  ) {
    return true;
  } else {
    return false;
  }
}

copy_key_button.addEventListener("click", (e) => {
  e.preventDefault();
  console.log("test");
});
