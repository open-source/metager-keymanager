document.querySelector("#micropayment-checkout > form button").addEventListener("click", () => {
    let revocationCheckbox = document.getElementById("revocation");
    if (!revocationCheckbox.checked) {
        document.querySelector("#micropayment-checkout > form input#revocation + label").style.borderLeft = "1px solid red";
        document.querySelector("#micropayment-checkout > form input#revocation + label").style.paddingLeft = "0.5rem";
    }
});

document.querySelector("#micropayment-checkout .revocation-required-error").style.display = "none";