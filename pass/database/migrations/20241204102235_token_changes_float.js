/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    knex.schema.alterTable("token_changes", table => {
        table.float("tokens").notNullable().alter();
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    knex.schema.alterTable("token_changes", table => {
        table.integer("tokens").notNullable().alter();
    });
};
