/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.alterTable("receipts", table => {
        table.string("invoice_id");
        table.dropColumn("company");
        table.dropColumn("name");
        table.dropColumn("email");
        table.dropColumn("address");
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.alterTable("receipts", table => {
        table.dropColumn("invoice_id");
        table.string("company", 500);
        table.string("name", 500).notNullable();
        table.string("email", 200).notNullable();
        table.text("address").notNullable();
    });
};
