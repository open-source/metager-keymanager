/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema
    .createTable("payment_references", (table) => {
      table.increments("id").primary();
      table.integer("amount", 5).notNullable();
      table.decimal("price", 6, 2).notNullable();
      table.string("key", 36).notNullable();
      table.dateTime("created_at", { useTz: false }).notNullable();
      table.dateTime("expires_at", { useTz: false }).notNullable();

      table.index(["id"]);
      table.index(["created_at", "price"]);
      table.index(["expires_at", "price"]);
    })
    .createTable("payments", (table) => {
      table.increments("id").primary();
      table.decimal("price", 6, 2).notNullable();
      table.decimal("converted_price", 6, 2).notNullable();
      table.string("converted_currency", 3).notNullable();
      table.integer("payment_reference_id");
      table
        .string("payment_processor", 20)
        .notNullable()
        .comment("The Payment Processor name");
      table.string("payment_processor_id").unique();
      table
        .text("payment_processor_data")
        .comment(
          "Stores arbitrary Data from the Payment Processor in JSON format"
        );
      table.integer("receipt_id").unique();
      table.dateTime("created_at", { useTz: false }).notNullable();

      table
        .foreign("payment_reference_id")
        .references("id")
        .inTable("payment_references")
        .onDelete("CASCADE");

      table.index(["id", "payment_reference_id", "price"]);
      table.index(["payment_processor"]);
    })
    .createTable("receipts", (table) => {
      table.increments("id");
      table.string("company", 500);
      table.string("name", 500).notNullable();
      table.string("email", 200).notNullable();
      table.text("address").notNullable();
      table.binary("receipt");
      table.integer("payment_id").notNullable().unique();
      table.dateTime("created_at", { useTz: false }).notNullable();

      table
        .foreign("payment_id")
        .references("id")
        .inTable("payments")
        .onDelete("CASCADE");
      table.index(["id", "created_at", "name", "email", "address"]);
    })
    .createTable("token_changes", (table) => {
      table.increments("id");
      table.integer("tokens").notNullable();
      table.integer("payment_reference_id").notNullable();
      table.dateTime("created_at", { useTz: false }).notNullable();

      table
        .foreign("payment_reference_id")
        .references("id")
        .inTable("payment_references")
        .onDelete("CASCADE");
      table.index(["id"]);
      table.index(["created_at", "tokens"]);
      table.index(["payment_reference_id"]);
    })
    .alterTable("payments", (table) => {
      table
        .foreign("receipt_id")
        .references("id")
        .inTable("receipts")
        .onDelete("CASCADE");
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema
    .alterTable("payments", (table) => {
      table.dropForeign("receipt_id");
    })
    .dropTable("token_changes")
    .dropTable("receipts")
    .dropTable("payments")
    .dropTable("payment_references");
};
