var express = require("express");
var router = express.Router({ mergeParams: true });

const config = require("config");
const { body, validationResult } = require("express-validator");
const Receipt = require("../../app/Receipt");
const Zammad = require("../../app/Zammad");
const Payment = require("../../app/Payment");
const dayjs = require("dayjs");

router.get("/", async (req, res) => {
  req.data.order.invoice = {
    params: {
      name: req.query.name || "",
      email: req.query.email || "",
      address: req.query.address || "",
    },
    create_invoice_url: `${
      res.locals.baseDir
    }/key/${req.data.key.key.get_key()}/orders/${
      req.data.order.payment.public_id
    }/invoice/create`,
    errors: {},
  };

  let orders = req.data.key.key.get_charge_orders();
  let payment_reference_ids = [];
  orders.forEach((order) => {
    payment_reference_ids.push(order.payment_reference_id);
  });

  // Populate Invoice Data from older orders
  req.data.order.invoice_old = {};
  let receipt = await Receipt.GET_LATEST_RECEIPT(payment_reference_ids);
  if (receipt != null) {
    await fetch(
      config.get("app.invoiceninja.url") +
        "/api/v1/invoices/" +
        receipt.invoice_id,
      {
        method: "GET",
        headers: {
          "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((invoice) => {
        return fetch(
          config.get("app.invoiceninja.url") +
            "/api/v1/clients/" +
            invoice.data.client_id,
          {
            method: "GET",
            headers: {
              "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
              "Content-Type": "application/json",
            },
          }
        );
      })
      .then((response) => response.json())
      .then((client) => {
        req.data.order.invoice_old = {
          company: client.data.name,
          first_name: client.data.contacts[0].first_name,
          last_name: client.data.contacts[0].last_name,
          address1: client.data.address1,
          address2: client.data.address2,
          zip: client.data.postal_code,
          city: client.data.city,
          state: client.data.state,
        };
      })
      .catch((reason) => {});
  }

  res.render("key", req.data);
});

router.post(
  "/*",
  body("company").isLength({ max: 100 }),
  body("first_name").isLength({ max: 50 }),
  body("last_name").isLength({ max: 50 }),
  body("address1").isLength({ max: 100 }),
  body("address2").isLength({ max: 100 }),
  body("zip").isLength({ max: 25 }),
  body("city").isLength({ max: 25 }),
  body("country").isLength({ max: 50 }),
  (req, res, next) => {
    req.data.order.invoice = {
      params: {
        company: req.body.company || "",
        first_name: req.body.first_name || "",
        last_name: req.body.last_name || "",
        address1: req.body.address1 || "",
        address2: req.body.address2 || "",
        zip: req.body.zip || "",
        city: req.body.city || "",
        state: req.body.state || "",
      },
      errors: {},
    };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        req.data.order.invoice.errors[error.param] = error.msg;
      });
      res.render("key", req.data);
      return;
    }
    next();
  }
);

router.post("/", async (req, res) => {
  let order_id = req.data.order.payment.public_id;
  let payment_reference = req.data.order.payment_reference.public_id;

  let url =
    req.data.links.order_actions_base +
    "/" +
    req.data.order.payment.public_id +
    "/receipt/download";

  // Check if there already is a receipt
  if (req.data.order.payment.receipt_id != null) {
    res.redirect(url);
    return;
  }

  // Check if there is a previous client
  let orders = req.data.key.key.get_charge_orders();
  let payment_reference_ids = [];
  orders.forEach((order) => {
    payment_reference_ids.push(order.payment_reference_id);
  });

  // Create a new Client
  let post_data = {
    contacts: [
      {
        first_name: req.data.order.invoice.params.first_name,
        last_name: req.data.order.invoice.params.last_name,
        send_email: true,
      },
    ],
    name:
      req.data.order.invoice.params.company.length > 0
        ? req.data.order.invoice.params.company
        : null,
    address1: req.data.order.invoice.params.address1,
    address2: req.data.order.invoice.params.address2,
    city: req.data.order.invoice.params.city,
    state: req.data.order.invoice.params.state,
    postal_code: req.data.order.invoice.params.zip,
    country_code: "DE",
    currency_code: "EUR",
    group_settings_id: config.get("app.invoiceninja.group_id"),
  };

  let receipt = await Receipt.GET_LATEST_RECEIPT(payment_reference_ids);
  let client_id = null;
  if (receipt != null) {
    await fetch(
      config.get("app.invoiceninja.url") +
        "/api/v1/invoices/" +
        receipt.invoice_id,
      {
        method: "GET",
        headers: {
          "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((invoice) => {
        return fetch(
          config.get("app.invoiceninja.url") +
            "/api/v1/clients/" +
            invoice.data.client_id,
          {
            method: "GET",
            headers: {
              "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
              "Content-Type": "application/json",
            },
          }
        );
      })
      .then((response) => response.json())
      .then((client) => {
        post_data = { ...client.data, ...post_data };
        client_id = client.data.id;
        post_data.contacts[0].id = client.data.contacts[0].id;
      })
      .catch((reason) => {});
  }

  if (req.lng.indexOf("de") != 0) {
    post_data.settings = {
      language_id: 1,
    }; // Set language to en
  } else {
    post_data.settings = {};
  }

  let create_or_update_promise = null;
  if (client_id != null) {
    // Update
    create_or_update_promise = fetch(
      config.get("app.invoiceninja.url") + "/api/v1/clients/" + client_id,
      {
        method: "PUT",
        headers: {
          "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(post_data),
      }
    );
  } else {
    create_or_update_promise = fetch(
      config.get("app.invoiceninja.url") + "/api/v1/clients",
      {
        method: "POST",
        headers: {
          "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(post_data),
      }
    );
  }

  return create_or_update_promise
    .then((response) => response.json())
    .then((response) => {
      let client = response.data;
      client.assigned_user_id = client.contacts[0].id;
      if (client_id == null) {
        return fetch(
          config.get("app.invoiceninja.url") + "/api/v1/clients/" + client.id,
          {
            method: "PUT",
            headers: {
              "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
              "Content-Type": "application/json",
            },
            body: JSON.stringify(client),
          }
        )
          .then((response) => response.json())
          .then((response) => response.data);
      } else {
        return client;
      }
    })
    .then((client) => {
      Payment.LOAD_FROM_PUBLIC_ID(req.data.order.payment.public_id).then(
        (payment) => {
          // Create the invoice
          let actions = new URLSearchParams({
            send_email: "false",
            mark_sent: "true",
            amount_paid: payment.converted_price,
          });
          return fetch(
            config.get("app.invoiceninja.url") +
              "/api/v1/invoices?" +
              actions.toString(),
            {
              method: "POST",
              headers: {
                "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                client_id: client.id,
                po_number: req.data.order.payment.public_id,
                due_date: dayjs().format("YYYY-MM-DD"),
                line_items: [
                  {
                    quantity: 1,
                    cost: req.data.order.payment_reference.price,
                    notes: req.t("subject", {
                      ns: "invoice",
                      amount: Math.round(
                        (payment.converted_price /
                          req.data.order.payment_reference.price) *
                          req.data.order.payment_reference.amount
                      ),
                      payment_reference:
                        req.data.order.payment_reference.public_id,
                    }),
                    public_notes: req.t("payment-received", {
                      ns: "invoice",
                    }),
                    tax_name1: "MwSt.",
                    tax_rate1: 7,
                  },
                ],
              }),
            }
          )
            .then((response) => response.json())
            .then((invoice) => {
              // Return the new invoice as pdf
              return Receipt.CREATE_NEW_RECEIPT({
                invoice_id: invoice.data.id,
                payment_id: req.data.order.payment.id,
              });
            })
            .then((receipt) => {
              payment.setReceipt(receipt.id);
              return res.redirect(url);
            });
        }
      );
    })
    .catch((reason) => {
      console.error(reason);
      res.redirect(
        req.data.links.order_actions_base +
          "/" +
          req.data.order.payment.public_id +
          "/receipt"
      );
    });

  let moderation_params = {
    order: req.data.order.payment.public_id,
    payment_reference: req.data.order.payment_reference.public_id,
    company: req.data.order.invoice.params.company,
    name: req.data.order.invoice.params.name,
    email: req.data.order.invoice.params.email,
    address: req.data.order.invoice.params.address,
  };

  req.data.order.invoice.params = moderation_params;
  req.data.order.invoice.moderation_url = `${
    res.locals.baseDir
  }/admin/payments/receipt?${new URLSearchParams(
    moderation_params
  ).toString()}`;

  // Render the message
  let ejs = require("ejs"),
    fs = require("fs"),
    template = fs.readFileSync(
      `${__dirname}/../../views/orders/invoice_message.ejs`,
      "utf-8"
    );

  let message = ejs.render(template, req.data);

  return Zammad.CREATE_RECEIPT_TICKET(
    moderation_params.name,
    moderation_params.email,
    message,
    req.data.order.payment.public_id
  )
    .then((success) => {
      if (success) {
        req.data.order.invoice.success = true;
        res.render("key", req.data);
      } else {
        console.error(response.body);
        throw "Fehler beim Erstellen der Benachrichtigung,";
      }
    })
    .then((response) => {
      if (response.status != 201) {
        console.error(response.body);
        throw "Fehler beim Erstellen der Benachrichtigung,";
      } else {
        req.data.order.invoice.success = true;
        res.render("key", req.data);
      }
    })
    .catch((reason) => {
      console.log(reason);
      req.data.order.invoice.errors["send_email"] =
        "Fehler beim Erstellen der Benachrichtigung,";
      res.render("key", req.data);
    });
});

router.get("/download", (req, res) => {
  if (req.data.order.payment.receipt_id === null) {
    res.locals.error = { status: 404 };
    res.locals.message = "Receipt not found";
    res.status(404).render("error");
  }
  return Receipt.LOAD_RECEIPT_FROM_INTERNAL_ID(
    req.data.order.payment.receipt_id
  )
    .then((receipt) => {
      // Receipt can either be in the database itself, or via invoiceninja
      if (receipt.invoice_id != null) {
        // Load invoice
        let invoice_number = null;
        return fetch(
          config.get("app.invoiceninja.url") +
            "/api/v1/invoices/" +
            receipt.invoice_id,
          {
            method: "GET",
            headers: {
              "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
              "Content-Type": "application/json",
            },
          }
        )
          .then((response) => response.json())
          .then((invoice) => {
            invoice_number = invoice.data.number;
            return fetch(
              config.get("app.invoiceninja.url") +
                "/api/v1/invoice/" +
                invoice.data.invitations[0].key +
                "/download",
              {
                method: "GET",
                headers: {
                  "X-API-TOKEN": config.get("app.invoiceninja.api_token"),
                  "Content-Type": "application/json",
                },
              }
            );
          })
          .then((response) => response.blob())
          .then((blob) => {
            res.setHeader("Content-Length", blob.size);
            res.setHeader("Content-Type", blob.type);
            res.setHeader(
              "Content-Disposition",
              `attachment; filename=receipt-metager-${invoice_number}.pdf`
            );
            return blob.arrayBuffer().then((buf) => {
              res.send(Buffer.from(buf));
            });
          });
      } else {
        res
          .type("pdf")
          .header({
            "Content-Disposition": `inline; filename=${receipt.public_id}.pdf`,
          })
          .send(Buffer.from(receipt.receipt.toString(), "base64"));
      }
    })
    .catch((reason) => {
      console.error(reason);
      res.locals.error = { status: 404 };
      res.locals.message = "Receipt not found";
      res.status(404).render("error");
    });
});

module.exports = router;
