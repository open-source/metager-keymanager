var express = require("express");
var router = express.Router({ mergeParams: true });

const config = require("config");
const Key = require("../../app/Key");
const Zammad = require("../../app/Zammad");

// Base URL: /key/:key/orders/:order/refund
router.use("/", (req, res, next) => {
  let refund_count = req.data.order.payment_reference.key.get_charge(req.data.order.payment_reference.id);
  let payment_count = Math.round((req.data.order.payment.price / req.data.order.payment_reference.price) * req.data.order.payment_reference.amount);
  req.data.order.refund = {
    count: refund_count,
    payment_count: payment_count,
    amount: ((refund_count / payment_count) * req.data.order.payment.price).toFixed(2),
  };
  req.data.css.push(`${res.locals.baseDir}/styles/orders/refund.css`);
  next();
});
router.get("/", (req, res) => {
  res.render("key", req.data);
});
router.post("/", (req, res, next) => {
  req.data.order.refund.message = req.body.message;

  // Refund values as sent back to us by the user.
  // Used only to verify the amount we are refunding corresponds to what we showed the user
  let user_count = parseInt(req.body.count); // Amount of search request discharged from key

  if (req.data.order.refund.count !== user_count) {
    // Something changed abort here
    req.data.order.refund.error = "invalid_data";
    res.render("key", req.data);
  } else if (req.data.order.refund.count <= 0) {
    req.data.order.refund.error = "refund_already_requested";
    res.render("key", req.data);
  } else {
    // Render the message
    let ejs = require("ejs"),
      fs = require("fs"),
      template = fs.readFileSync(
        `${__dirname}/../../views/orders/refund_message.ejs`,
        "utf-8"
      );

    let message = ejs.render(template, req.data);

    return Zammad.CREATE_REFUND_TICKET(message, req.data.order.payment.public_id).then(success => {
      if (success) {
        return Key.GET_KEY(req.data.key.key.get_key(), true);
      } else {
        return Promise.reject("Fehler beim Erstellen der Benachrichtigung,");
      }
    }).then((key) => {
      key.discharge_key(req.data.order.refund.count, req.data.order.payment_reference.id);
      return key.save();
    })
      .then((new_key) => {
        req.data.key.key = new_key;
        req.data.order.refund.success = true;
        res.render("key", req.data);
      })
      .catch((reason) => {
        console.log(reason);
        req.data.order.refund.error = "send_email";
        res.render("key", req.data);
      });
  }
});

router.post(
  "/process",
  (req, res, next) => {
    if (!req.data.admin) {
      res.locals.error = { status: 401 };
      res.locals.message = "Unauthorized";
      res.render("error");
    } else {
      next();
    }
  },
  (req, res) => {
    if (req.body.action === "approve") {
      req.data.order.order
        .refund()
        .then(() => {
          req.data.order.refund.result = "REFUNDED";
          res.render("key", req.data);
        })
        .catch((reason) => {
          console.debug(reason);
          res.locals.error = { status: 400 };
          res.locals.message = "Error while executing refund";
        });
    } else {
      req.data.order.order.denyRefund().then(() => {
        req.data.order.refund.result = "REFUND_DENIED";
        res.render("key", req.data);
      });
    }
  }
);

module.exports = router;
