var express = require("express");
var router = express.Router({ mergeParams: true });

const {
  param,
  body,
  validationResult,
  matchedData,
} = require("express-validator");
const OrderReceipt = require("../../app/pdf/OrderReceipt");
const PaymentReference = require("../../app/PaymentReference");
const Payment = require("../../app/Payment");

let refundRouter = require("./refund");

router.use("/", (req, res, next) => {
  if (typeof req.cookies.payment_reference !== "undefined") {
    req.data.form = {
      payment_reference: req.cookies.payment_reference,
    };
  }
  next();
});
router.get("/", (req, res) => {
  req.data.page = "order";
  req.data.css.push(`${res.locals.baseDir}/styles/orders/orders.css`);
  res.render("key", req.data);
});

router.post(
  "/",
  body("payment_reference").matches(/^(Z)?(\d+)$/),
  (req, res) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.data.page = "order";
      req.data.css.push(`${res.locals.baseDir}/styles/orders/orders.css`);
      req.data.error = 400;
      req.data.form = {
        "order-id": req.body["order-id"],
      };
      res.render("key", req.data);
      return;
    }
    let queryData = matchedData(req, { location: ["body"] });
    PaymentReference.CONVERT_TO_INTERNAL_ID(queryData.payment_reference).then(
      (internal_payment_reference) => {
        Payment.LOAD_PAYMENTS_FROM_REFERENCE(
          internal_payment_reference,
          req.data.key.key.get_key()
        ).then((payments) => {
          if (payments.length === 0) {
            req.data.page = "order";
            req.data.css.push(`${res.locals.baseDir}/styles/orders/orders.css`);
            req.data.error = 404;
            req.data.form = {
              "order-id": req.body["order-id"],
            };
            res.render("key", req.data);
          } else {
            res.redirect(
              `${res.locals.baseDir}/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference
              }#order`
            );
          }
        });
      }
    );
  }
);

router.use(
  "/:payment_reference",
  param("payment_reference")
    .matches(/^(Z)?(\d+)$/)
    .customSanitizer((payment_reference, { req }) =>
      PaymentReference.LOAD_FROM_PUBLIC_ID(
        payment_reference,
        req.data.key.key.get_key()
      )
    ),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(404).render("error", {
        message: "Order not found",
        error: {
          status: 404,
        },
      });
    }
    let queryData = matchedData(req, { locations: ["params"] });
    req.data.order = {
      payment_reference: queryData.payment_reference,
    };
    return Payment.LOAD_PAYMENTS_FROM_REFERENCE(
      req.data.order.payment_reference.id,
      req.data.key.key.get_key()
    )
      .then((payments) => {
        let url = new URL(
          `${res.locals.baseDir}/key/${req.data.key.key.get_key()}/orders`
        );
        res.cookie("payment_reference", queryData.payment_reference.public_id, {
          path: url.pathname,
        });
        req.data.page = "order";
        req.data.order.payment_reference = queryData.payment_reference;
        req.data.order.payments = payments;

        req.data.css.push(`${res.locals.baseDir}/styles/orders/order.css`);
        req.data.links.order_url = `${res.locals.baseDir
          }/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference.public_id
          }#order`;
        req.data.links.order_actions_base = `${res.locals.baseDir
          }/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference.public_id
          }`;
        req.data.links.receipt_url = `${res.locals.baseDir
          }/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference.public_id
          }/pdf`;
        req.data.links.invoice_url = `${res.locals.baseDir
          }/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference.public_id
          }/invoice#invoice-form`;
        req.data.links.refund_url = `${res.locals.baseDir
          }/key/${req.data.key.key.get_key()}/orders/${queryData.payment_reference.public_id
          }/refund#refund-form`;
        /*if (req.data.order.order.isReceiptCreated()) {
        req.data.order.download_invoice_url = `${res.locals.baseDir}/key/${req.data.key.key.get_key()
          }/orders/${req.data.order.order.getOrderID()}/invoice/download`;
      }*/
        next("route");
      })
      .catch((reason) => {
        console.error(reason);
        res.status(404).render("error", {
          message: "Order not found",
          error: {
            status: 404,
          },
        });
        return;
      });
  }
);

router.get("/:payment_reference", (req, res) => {
  res.render("key", req.data);
});

router.use("/:payment_reference/:order_id", (req, res, next) => {
  Payment.LOAD_FROM_PUBLIC_ID(req.params.order_id).then((payment) => {
    req.data.order.payment = payment;
    next();
  });
});

router.get("/:payment_reference/:order_id/pdf", (req, res) => {
  return Payment.LOAD_FROM_PUBLIC_ID(req.params.order_id)
    .then((payment) => {
      return OrderReceipt.CREATE_ORDER_RECEIPT(
        req.data.order.payment_reference,
        payment,
        null,
        req.t
      )
        .then((data) => {
          res
            .status(200)
            .header({
              "Content-Type": "application/pdf",
              "Content-Disposition": `inline, filename=${req.params.order_id}.pdf`,
            })
            .send(Buffer.concat(data));
        })
        .catch((reason) => {
          res.locals.error = {
            status: 500,
            stack: JSON.stringify(reason, null, 4),
          };
          res.locals.message = "Error generating Order Receipt";
          res.status(500).render("error");
        });
    })
    .catch((reason) => {
      console.error(reason);
      res.status(404).render("error", {
        error: {
          status: 404,
        },
        message: "Payment not found",
      });
    });
});

let receiptRouter = require("./receipt");
router.use("/:payment_reference/:order_id/receipt", receiptRouter);
router.use("/:payment_reference/:order_id/refund", refundRouter);

module.exports = router;
