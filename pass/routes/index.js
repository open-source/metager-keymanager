var express = require("express");
var router = express.Router({ mergeParams: true });

var lessMiddleware = require("less-middleware");
var browserify = require("browserify-middleware");
var paypalCheckoutRouter = require("../routes/checkout/paypal.js");
var micropaymentCheckoutRouter = require("../routes/checkout/micropayment");
var apiRouter = require("./api");
var adminRouter = require("./admin/index");
var helpRouter = require("./help");
var path = require("path");

const config = require("config");

router.use(lessMiddleware(path.join(__dirname, "..", "public")));
router.use(express.static(path.join(__dirname, "..", "public")));

router.use("/api/json", apiRouter);

router.use("/admin", adminRouter);

router.use("/help", helpRouter);

/* GET home page. */
router.get("/", function (req, res) {
  req.i18n.setDefaultNamespace("index"); // Default NS for localized Strings
  res.render("index");
});

router.get("/agb", (req, res) => {
  res.render("agb");
});

router.get("/cost", (req, res) => {
  res.locals.price = config.get("price");
  res.render("cost");
});

var keyRouter = require("../routes/key");
router.use("/key", keyRouter);

router.use("/webhooks/paypal", paypalCheckoutRouter);
router.use("/webhooks/micropayment", micropaymentCheckoutRouter);

// Browserified Javascript files
router.get(
  "/js/base.js",
  browserify(path.join(__dirname, "..", "resources", "js", "base.js"))
);
router.get(
  "/js/funding_sources.js",
  browserify(
    path.join(__dirname, "..", "resources", "js", "funding_sources.js")
  )
);
router.get(
  "/js/checkout_paypal.js",
  (req, res, next) => {
    res.header({ "Access-Control-Allow-Origin": "https://www.paypal.com" });
    next();
  },
  browserify(
    path.join(__dirname, "..", "resources", "js", "checkout_paypal.js")
  )
);
router.get(
  "/js/enter.js",
  browserify(path.join(__dirname, "..", "resources", "js", "enter.js"))
);
router.get(
  "/js/chat.js",
  browserify(path.join(__dirname, "..", "resources", "js", "chat.js"))
);

module.exports = router;
