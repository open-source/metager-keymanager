var express = require("express");
var router = express.Router({ mergeParams: true });

const config = require("config");
const PaymentReference = require("../../app/PaymentReference.js");
const Paypal = require("../../app/payment_processor/Paypal.js");
const { body } = require("express-validator");

router.use("/", async (req, res, next) => {
  if (req.data && req.data.checkout) {
    req.data.checkout.payment = {
      provider: "paypal",
      paypal: {
        client_id: config.get(`payments.paypal.client_id`),
      },
    };
  }
  new Paypal()
    .verifyWebhook()
    .then(() => {
      next("route");
    })
    .catch((reason) => {
      console.error(reason);
      next("route");
    });
});

router.get("/:funding_source", async (req, res) => {
  req.data.checkout.payment.paypal.funding_source = req.params.funding_source;

  res.header({ "Access-Control-Allow-Origin": "https://www.paypal.com" });
  if (req.params.funding_source === "card") {
    let client_token = await new Paypal().generateClientToken();
    req.data.checkout.payment.paypal.client_token = client_token;
  }

  req.data.change_url.funding_source_not_eligible =
    `${res.locals.baseDir}/key/` +
    encodeURIComponent(req.data.key.key.get_key()) +
    "/checkout/" +
    encodeURIComponent(req.data.checkout.amount) +
    "?error=funding_source_not_eligible";

  req.data.change_url.order_base_url =
    `${res.locals.baseDir}/key/` +
    encodeURIComponent(req.data.key.key.get_key()) +
    "/checkout/" +
    encodeURIComponent(req.data.checkout.amount) +
    "/paypal/" +
    encodeURIComponent(req.data.checkout.payment.paypal.funding_source) +
    "/order";

  req.data.js.push(`${res.locals.baseDir}/js/checkout_paypal.js`);
  res.render("key", req.data);
});

router.post("/:funding_source/order/create", async (req, res) => {
  let funding_source = req.params.funding_source;
  // Order data is validated: Create and store the PaymentReference
  let paypal = new Paypal();
  return PaymentReference.CREATE_NEW_REQUEST(
    req.params.amount,
    req.data.key.key.get_key()
  )
    .then((payment_reference) => {
      return paypal.createOrder(payment_reference, funding_source == "card", req.t).then((paypal) => {
        return __redis_client
          .setex(
            `payments:paypal:${payment_reference.public_id}`,
            600,
            paypal.getOrderId()
          )
          .then(() => {
            return res.status(200).json({
              payment_reference: payment_reference.public_id,
              paypal_order_id: paypal.getOrderId(),
            });
          });
      });
    })
    .catch((reason) => {
      console.error(reason);
      res.status(400).json({
        errors: [{ msg: "Failed to create a new Order. Try again later" }],
      });
    });
});

router.post("/:funding_source/order/cancel", async (req, res) => {
  PaymentReference.LOAD_FROM_PUBLIC_ID(req.body.payment_reference)
    .then((payment_reference) =>
      payment_reference.delete(req.data.key.key.get_key())
    )
    .then(() =>
      __redis_client.del(`payments:paypal:${req.body.payment_reference}`)
    )
    .then(() => {
      res.status(200).json({ msg: "Order deleted" });
    })
    .catch((reason) => {
      console.error(reason);
      res.status(400).json({ msg: "Failed to load/cancel Order." });
    }); // Deletes a order but only if the payment is not yet completed
});

// We need to make sure that a card payment was authorized with 3-DS.
// All other PayPal captures will be handled in the next capture rotue
router.use(
  "/card/order/capture",
  body("payment_reference").matches(/^(Z)?(\d+)$/),
  async (req, res, next) => {
    __redis_client
      .get(`payments:paypal:${req.body.payment_reference}`)
      .then((paypal_order_id) => {
        let paypal = new Paypal(paypal_order_id);
        return paypal.verify_3D();
      })
      .then(() => next("route"))
      .catch((reason) => {
        console.error(reason);
        res.status(400).json({
          errors: [
            { type: "PAYPAL_CARD_3D_ERROR", msg: "3D Verification Failed" },
          ],
        });
      });
  }
);

// capture payment & store order information or fullfill order
router.post(
  "/:funding_source/order/capture",
  body("payment_reference").matches(/^(Z)?(\d+)$/),
  async (req, res) => {
    return PaymentReference.LOAD_FROM_PUBLIC_ID(req.body.payment_reference)
      .then((payment_reference) => {
        return __redis_client
          .get(`payments:paypal:${payment_reference.public_id}`)
          .then((paypal_order_id) => {
            let paypal = new Paypal(paypal_order_id);
            return paypal.captureOrder();
          })
          .then((response_data) => {
            let payment_promises = [];
            for (let i = 0; i < response_data.purchase_units.length; i++) {
              let purchase_unit = response_data.purchase_units[i];
              for (let j = 0; j < purchase_unit.payments.captures.length; j++) {
                let capture = purchase_unit.payments.captures[j];
                if (
                  capture.status !== "COMPLETED" ||
                  capture.amount.currency_code !== "EUR"
                ) {
                  continue;
                }
                payment_promises.push(
                  payment_reference.createPayment({
                    price: parseFloat(capture.amount.value),
                    converted_price: parseFloat(capture.amount.value),
                    converted_currency: capture.amount.currency_code,
                    payment_reference_id: payment_reference.id,
                    payment_processor: Paypal.NAME,
                    payment_processor_id: capture.id,
                    payment_processor_data: capture,
                  })
                );
              }
            }
            return Promise.all(payment_promises);
          })
          .then(() => {
            let redirect_url =
              `${res.locals.baseDir}/key/` +
              req.data.key.key.get_key() +
              "/orders/" +
              payment_reference.public_id;
            res.status(200).json({
              redirect_url: redirect_url,
            });
          });
      })
      .catch((reason) => {
        console.debug(reason);
        res.status(400).json(reason);
      });
  }
);

router.post("/webhook", async (req, res) => {
  // Verify that the webhook came from paypal
  let paypal = new Paypal();
  return paypal
    .processWebhook(
      req.headers["paypal-auth-algo"],
      req.headers["paypal-cert-url"],
      req.headers["paypal-transmission-id"],
      req.headers["paypal-transmission-sig"],
      req.headers["paypal-transmission-time"],
      req.body
    )
    .then(() => {
      return res.status(200).send("");
    })
    .catch((reason) => {
      console.debug(reason);
      return res.status(200).send("");
    });
});

module.exports = router;

//////////////////////

// PayPal API helpers

//////////////////////

// generate an access token using client id and app secret
