var express = require("express");
const PaymentReference = require("../../app/PaymentReference");
var router = express.Router({ mergeParams: true });

router.use("/", (req, res, next) => {
  if (req.data && req.data.checkout) {
    req.data.checkout.payment = {
      provider: "manual",
    };
  }
  req.data.css.push(`${res.locals.baseDir}/styles/key/checkout-manual.css`);
  // Make sure only development environments can use this routes
  if (process.env.NODE_ENV !== "development") {
    throw "Manual Payments only allowed in development environment.";
  } else {
    next();
  }
});

router.get("/", (req, res) => {
  res.render("key", req.data);
});

router.post("/", (req, res) => {
  return PaymentReference.CREATE_NEW_REQUEST(
    req.params.amount,
    req.data.key.key.get_key()
  )
    .then((payment_reference) => payment_reference.chargeKey())
    .then(() => {
      let redirect_url =
        `${res.locals.baseDir}/key/` + req.data.key.key.get_key();
      res.redirect(redirect_url);
    });
});

module.exports = router;
