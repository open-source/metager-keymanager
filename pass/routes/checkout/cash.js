var express = require("express");
var router = express.Router({ mergeParams: true });
const { query, validationResult, matchedData } = require("express-validator");
const dayjs = require("dayjs");
const PaymentReference = require("../../app/PaymentReference");

router.use("/", (req, res, next) => {
  if (req.data && req.data.checkout) {
    req.data.checkout.payment = {
      provider: "cash",
    };
  }
  req.data.css.push(`${res.locals.baseDir}/styles/key/checkout-cash.css`);
  next();
});

router.get("/", async (req, res) => {
  res.render("key", req.data);
});

router.post("/", async (req, res) => {
  return PaymentReference.CREATE_NEW_REQUEST(
    req.data.checkout.amount,
    req.data.key.key.get_key(),
    dayjs().add(2, "month")
  )
    .then(async (payment_reference) => {
      req.data.checkout.payment_reference = payment_reference;
      res.render("key", req.data);
    })
    .catch((reason) => {
      console.error(reason);
      res.render("key", req.data);
    });
});

router.use((next) => {
  next();
});

module.exports = router;
