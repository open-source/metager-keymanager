var express = require("express");
var router = express.Router({ mergeParams: true });

const config = require("config");
const PaymentReference = require("../../app/PaymentReference");
const Micropayment = require("../../app/payment_processor/Micropayment");
const crypto = require("crypto");

router.get("/event", (req, res) => {
  Micropayment.VERIFY_WEBHOOK(req.query)
    .then(() =>
      PaymentReference.LOAD_FROM_PUBLIC_ID(req.query.paymentreference)
    )
    .then((payment_reference) => {
      let price = (req.query.amount / 100).toFixed(2);

      let redirect_url = new URL(
        `${res.locals.baseDir}/key/` +
        payment_reference.key.get_key() +
        "/orders/" +
        payment_reference.public_id
      );

      // Prepay payment unused events
      if (["billing"].includes(req.query.function) && (req.query.service != "prepay" || ["PAID", "OVERPAID"].includes(req.query.paystatus))) {
        return payment_reference
          .createPayment({
            price: price,
            converted_currency: req.query.currency,
            converted_price: price,
            payment_processor: Micropayment.NAME,
            payment_processor_id: req.query.auth,
            payment_processor_data: req.query,
          })
          .then(async payment => redirect_url);
      } else if (["refund", "storno", "quit"].includes(req.query.function) && (req.query.service != "prepay" || req.query.paystatus == "CLOSED")) {
        return payment_reference
          .createPayment({
            price: price * -1,
            converted_currency: req.query.currency,
            converted_price: price,
            payment_processor: Micropayment.NAME,
            payment_processor_id: req.query.auth + "_storno",
            payment_processor_data: req.query,
          })
          .then(async payment => redirect_url);
      } else {
        return redirect_url;
      }
    })
    .then((redirect_url) => {
      let response = {
        status: "ok",
        url: redirect_url.toString(),
        forward: req.query.service === "prepay" ? "0" : "1",
        target: "_self",
      };
      res.send(
        Object.entries(response)
          .map(([k, v]) => `${k}=${v}`)
          .join("\n")
      );
    })
    .catch((reason) => {
      console.error(reason);
      let response = {
        status: "error",
      };
      res.send(
        Object.entries(response)
          .map(([k, v]) => `${k}=${v}`)
          .join("\n")
      );
    });
});

router.use("/", (req, res, next) => {
  if (req.data && req.data.checkout) {
    req.data.checkout.payment = {
      provider: "micropayment",
    };
    req.data.css.push(
      `${res.locals.baseDir}/styles/key/checkout-micropayment.css`
    );
  }
  next();
});

router.use("/:service", (req, res, next) => {
  let subdomain = "";
  let servicename = "";

  let privacy_url = "";
  switch (req.params.service) {
    case "prepay":
      subdomain = "prepayment";
      servicename = "prepay";
      privacy_url = "https://resources.micropayment.de/billing/documents/privacy-policy/prepay/prepay-gmbh-de.pdf";
      privacy_url_text = "Micropayment";
      break;
    case "lastschrift":
      subdomain = "sepadirectdebit";
      servicename = "lastschrift";
      privacy_url = "https://resources.micropayment.de/billing/documents/privacy-policy/debit/debit-gmbh-de.pdf";
      privacy_url_text = "Micropayment";
      break;
    case "directbanking":
      subdomain = "directbanking";
      servicename = "sofort";
      privacy_url = "https://resources.micropayment.de/billing/documents/privacy-policy/sofort/sofort-gmbh-de.pdf";
      privacy_url_text = "Micropayment";
  }

  let payment_window_url = new URL(
    `https://${subdomain}.micropayment.de/${servicename}/event`
  );

  req.data.checkout.payment.micropayment = {
    funding_source: req.params.service,
    payment_window_url: payment_window_url,
    privacy_url: privacy_url,
    privacy_url_text: privacy_url_text
  };
  next();
});

router.get("/:service", (req, res) => {
  req.data.js.push(`${res.baseDir}/js/micropayment.js`);
  res.render("key", req.data);
});

router.post("/:service", async (req, res) => {
  return PaymentReference.CREATE_NEW_REQUEST(
    req.data.checkout.amount,
    req.data.key.key.get_key()
  ).then((payment_reference) => {
    /**
     * @type {URL}
     */
    let payment_window_url =
      req.data.checkout.payment.micropayment.payment_window_url;
    let searchParams = {
      project: config.get("payments.micropayment.project"),
      amount: payment_reference.price * 100,
      title: "MetaGer Schlüssel",
      mp_user_id: payment_reference.public_id,
      producttype: "quantity",
      paytext: `${payment_reference.amount} MetaGer Token (${payment_reference.public_id})`,
      testmode: process.env.NODE_ENV === "development" ? "1" : "0",
      paymentreference: payment_reference.public_id,
      service: req.data.checkout.payment.micropayment.funding_source,
      vatinfo: "1",
    };

    if (req.data.checkout.payment.micropayment.funding_source == "prepay") {
      searchParams.mp_user_email = req.body.email;
    }

    let seal = "";
    for (let key in searchParams) {
      seal += `${key}=${searchParams[key]}&`;
    }
    seal = seal.replace(/&$/, "");
    seal += config.get("payments.micropayment.access_key");

    let hasher = crypto.createHash("md5");
    seal = hasher.update(seal).digest("hex");
    searchParams.seal = seal;

    payment_window_url.search = new URLSearchParams(searchParams).toString();

    res.redirect(payment_window_url.toString());
  });
});

module.exports = router;
