var express = require("express");
var router = express.Router();

router.get("/faq", (req, res) => {
  res.render("help/faq");
});

router.get("/anonymous-token", (req, res) => {
  res.render("help/anonymous-token");
});

module.exports = router;
