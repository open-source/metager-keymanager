var express = require("express");
var router = express.Router();
const config = require("config");
const dayjs = require("dayjs");
const { auth } = require("express-openid-connect");
const {
  validationResult,
  matchedData,
  body,
  query,
  oneOf,
} = require("express-validator");
const OrderReceipt = require("../../app/pdf/OrderReceipt");
const crypto = require("crypto");
const Payment = require("../../app/Payment");
const PaymentReference = require("../../app/PaymentReference");
const Receipt = require("../../app/Receipt");
const Cash = require("../../app/payment_processor/Cash");
const Key = require("../../app/Key");
const Paypal = require("../../app/payment_processor/Paypal");
const Micropayment = require("../../app/payment_processor/Micropayment");

router.use((req, res, next) => {
  let cookie_path = new URL(res.locals.baseDir).pathname.replace(
    /(\/)?$/,
    "/admin"
  );
  auth({
    issuerBaseURL: `${config.get("app.openid_auth.url")}`,
    baseURL: res.locals.baseDir + "/admin",
    clientID: config.get("app.openid_auth.app_id"),
    clientSecret: config.get("app.openid_auth.app_secret"),
    secret: config.get("app.secret"),
    session: {
      absoluteDuration: 3600,
      rollingDuration: 600,
      signSessionStoreCookie: true,
      name: "keymanagerSession",
      cookie: {
        path: cookie_path,
      },
    },
    secret: config.get("app.secret"),
    authRequired: true,
    idpLogout: true,
  })(req, res, next);
});

router.get("/", (req, res) => {
  res.render("admin/index");
});

router.get(
  "/payments/receipt",
  query("order")
    .matches(/^(A)?(\d+)$/)
    .bail()
    .customSanitizer((orderid) => Payment.LOAD_FROM_PUBLIC_ID(orderid)),
  query("email").isEmail({ domain_specific_validation: true }),
  query("address").isLength({ max: 1000 }),
  query("name").isLength({ max: 500, min: 5 }),
  query("company").isLength({ max: 500 }),
  async (req, res) => {
    const reqData = matchedData(req, { locations: ["query"] });
    let data_complete = true;
    if ("order" in reqData) {
      res.locals.order = reqData.order;
      let payment_reference = await PaymentReference.LOAD_FROM_ID(
        reqData.order.payment_reference_id
      );
      res.locals.payment_reference = payment_reference;
    } else {
      data_complete = false;
    }

    if ("email" in reqData) {
      res.locals.email = reqData.email;
    } else {
      data_complete = false;
    }
    if ("address" in reqData) {
      res.locals.address = reqData.address;
    } else {
      data_complete = false;
    }
    if ("name" in reqData) {
      res.locals.name = reqData.name;
    } else {
      data_complete = false;
    }
    if ("company" in reqData) {
      res.locals.company = reqData.company;
    }

    if (reqData.order && reqData.order.receipt_id !== null) {
      // There is already a receipt: Send it back
      return Receipt.LOAD_RECEIPT_FROM_INTERNAL_ID(
        reqData.order.receipt_id
      ).then((receipt) => {
        let receipt_data = Buffer.from(receipt.receipt.toString(), "base64");
        res
          .header({
            "Content-Type": "application/pdf",
            "Content-Disposition": `inline; filename=${receipt.public_id}.pdf`,
          })
          .send(receipt_data);
      });
    }

    if (data_complete) {
      // Create Invoice for preview
      return OrderReceipt.CREATE_ORDER_RECEIPT(
        res.locals.payment_reference,
        reqData.order,
        new Receipt({
          id: -1,
          company: res.locals.company,
          name: res.locals.name,
          email: res.locals.email,
          address: res.locals.address,
          created_at: dayjs().format("YYYY-MM-DD HH:mm:ss"),
          payment_id: reqData.order.id,
        }),
        req.t
      )
        .then((receipt_pdf) => {
          receipt_pdf = Buffer.concat(receipt_pdf).toString("base64");
          res.locals.receiptb64 = receipt_pdf;
          let hasher = crypto.createHash("sha256");
          hasher.update(
            reqData.company +
            res.locals.name +
            res.locals.email +
            res.locals.address
          );
          res.locals.datahash = hasher.digest("hex");
          res.render("admin/payments/receipt");
        })
        .catch((reason) => {
          console.error(reason);
          res.render("admin/payments/receipt");
        });
    }
    res.render("admin/payments/receipt");
  }
);

router.post(
  "/payments/receipt",
  body("order")
    .matches(/^(A)?(\d+)$/)
    .customSanitizer((orderid) => Payment.LOAD_FROM_PUBLIC_ID(orderid)),
  body("email").isEmail({ domain_specific_validation: true }),
  body("address").isLength({ max: 1000 }),
  body("name").isLength({ max: 500, min: 5 }),
  body("company").isLength({ max: 500 }),
  body("datahash").isHash("sha256"),
  async (req, res) => {
    const reqData = matchedData(req, { locations: ["body"] });
    let data_complete = true;
    let url = new URL(`${res.locals.baseDir}/admin/payments/receipt`);
    let payment_reference;
    if (!("order" in reqData)) {
      data_complete = false;
    } else {
      url.searchParams.append("order", reqData.order.public_id);
      payment_reference = await PaymentReference.LOAD_FROM_ID(
        reqData.order.payment_reference_id
      );
    }
    if (!("email" in reqData)) {
      data_complete = false;
    } else {
      url.searchParams.append("email", reqData.email);
    }
    if (!("address" in reqData)) {
      data_complete = false;
    } else {
      url.searchParams.append("address", reqData.address);
    }
    if (!("name" in reqData)) {
      data_complete = false;
    } else {
      url.searchParams.append("name", reqData.name);
    }
    if ("company" in reqData) {
      url.searchParams.append("company", reqData.company);
    }

    if (!data_complete || reqData.order.receipt_id !== null) {
      res.redirect(url.toString());
      return;
    }

    let hasher = crypto.createHash("sha256");
    hasher.update(
      reqData.company + reqData.name + reqData.email + reqData.address
    );
    let hash = hasher.digest("hex");
    if (reqData.datahash !== hash) {
      res.redirect(url.toString());
      return;
    }

    return Receipt.CREATE_NEW_RECEIPT({
      company: reqData.company,
      name: reqData.name,
      email: reqData.email,
      address: reqData.address,
      payment_id: reqData.order.id,
    })
      .then((receipt) => {
        reqData.id = receipt.public_id;
        let receipt_data;
        return OrderReceipt.CREATE_ORDER_RECEIPT(
          payment_reference,
          reqData.order,
          receipt,
          req.t
        )
          .then((data) => {
            receipt_data = Buffer.concat(data);
            return receipt.attachReceipt(receipt_data.toString("base64"));
          })
          .then((receipt) => reqData.order.setReceipt(receipt.id))
          .then(() => {
            res
              .type("pdf")
              .header({
                "Content-Disposition": `inline; filename=${receipt.public_id}.pdf`,
              })
              .send(receipt_data);
          });
      })
      .catch((reason) => {
        console.error(reason);
        res.redirect(url.toString());
        return;
      });
    reqData.id = await OrderReceipt.CREATE_UNIQUE_RECEIPT_ID();
    OrderReceipt.CREATE_ORDER_RECEIPT(reqData.order, reqData, req.t)
      .then((data) => {
        receipt_pdf = Buffer.concat(data).toString("base64");
        return reqData.order.attachReceipt(receipt_pdf, "INV_" + reqData.id);
      })
      .then(() => {
        res
          .header({
            "Content-Type": "application/pdf",
            "Content-Disposition": `attachment; filename=INV_${reqData.order.getOrderID()}.pdf`,
          })
          .send(Buffer.from(receipt_pdf, "base64"));
      })
      .catch((reason) => {
        console.error(reason);
        res.redirect(url.toString());
        return;
      });
  }
);

router.use("/payments/cash", (req, res, next) => {
  res.locals.allowed_currencies = config.get("price.allowed_currencies");
  next();
});
router.get("/payments/cash", (req, res) => {
  res.render("admin/payments/cash");
});

router.post(
  "/payments/cash",
  body("payment_reference")
    .matches(/^(Z)?(\d+)/)
    .customSanitizer(async (payment_reference) =>
      PaymentReference.LOAD_FROM_PUBLIC_ID(payment_reference)
    ),
  body("converted_price").notEmpty().isFloat({ gt: 0 }).toFloat(),
  body("converted_currency").isIn(config.get("price.allowed_currencies")),
  body("price")
    .custom((price, { req }) => {
      if (req.body.converted_currency !== "EUR" && price.length === 0) {
        return Promise.reject("Price is mandatory!");
      }
      return true;
    })
    .toFloat(),
  async (req, res) => {
    let queryData = matchedData(req, { location: ["query"] });

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.locals.errors = errors;
      res.locals.oldData = queryData;
      res.render("admin/payments/cash");
      return;
    }

    let price_data = {
      price: queryData.price,
      converted_price: queryData.converted_price,
      converted_currency: queryData.converted_currency,
    };
    if (isNaN(queryData.price)) {
      price_data.price = queryData.converted_price;
    }

    /** @type {PaymentReference} */
    let payment_reference = queryData.payment_reference;
    return payment_reference
      .createPayment({
        price: price_data.price,
        converted_price: price_data.converted_price,
        converted_currency: price_data.converted_currency,
        payment_processor: Cash.NAME,
      })
      .then(() => {
        return payment_reference.getKey(false).then((key) => {
          return res.redirect(`${res.baseDir}/admin/key/${key.get_key()}`);
        });
      })
      .catch((reason) => {
        console.error(reason);
        res.locals.errors = errors;
        res.locals.oldData = queryData;
        res.render("admin/payments/cash");
        return;
      });
  }
);

router.get("/key", (req, res) => {
  res.render("admin/key/index");
});

router.post("/key", (req, res) => {
  if (req.body.payment_id.length > 0) {
    // Either Payment ID or PaymentReference ID can be Supplied
    if (req.body.payment_id.indexOf("A") == 0) {
      return Payment.LOAD_FROM_PUBLIC_ID(req.body.payment_id)
        .then(payment => {
          return PaymentReference.LOAD_FROM_ID(payment.payment_reference_id)
            .then(payment_reference => {
              return res.redirect(
                `${res.baseDir}/admin/key/${payment_reference.key.get_key()}`
              );
            })
        });
    } else {
      return PaymentReference.LOAD_FROM_PUBLIC_ID(req.body.payment_id)
        .then((payment_reference) => {
          return res.redirect(
            `${res.baseDir}/admin/key/${payment_reference.key.get_key()}`
          );
        })
        .catch((reason) => {
          console.error(reason);
          return res.redirect(`${res.baseDir}/admin/key`);
        });
    }
  } else {
    return Key.GET_KEY(req.body.key.trim(), false).then((key) => {
      return res.redirect(`${res.baseDir}/admin/key/${key.get_key()}`);
    });
  }
});

router.use("/key/:key", (req, res, next) => {
  if (req.query.charge_success) {
    res.locals.success = true;
  }
  Key.GET_KEY(req.params.key, false).then(async (key) => {
    res.locals.key = key;
    // Get Payments for this key
    res.locals.payments = [];

    await PaymentReference.LOAD_REFERENCES_FOR_KEY(key)
      .then(async charge_orders => {
        for (let i = 0; i < charge_orders.length; i++) {
          let charge_order = charge_orders[i];
          await Payment.LOAD_PAYMENTS_FROM_REFERENCE(
            charge_order.id,
            key.get_key()
          ).then((payments) => {
            for (j = 0; j < payments.length; j++) {
              if (payments[j].payment_processor instanceof Paypal) {
                payments[j].payment_processor_data = JSON.stringify(
                  JSON.parse(payments[j].payment_processor_data),
                  null,
                  4
                );
                let host = "www.paypal.com";
                if (process.env.NODE_ENV == "development") {
                  host = "sandbox.paypal.com";
                }
                payments[
                  j
                ].payment_processor_id = `<a href=\"https://${host}/activity/payment/${payments[j].payment_processor_id}\" target=\"_blank\">${payments[j].payment_processor_id}</a>`;
              } else if (payments[j].payment_processor instanceof Micropayment) {
                payments[j].payment_processor_data = JSON.stringify(JSON.parse(payments[j].payment_processor_data), null, 4);
                payments[j].payment_processor_id = `<a href=\"https://controlcenter.micropayment.de/app/statistic/sessionsearch?search_term=${payments[j].payment_processor_id.replace("_storno", "")}&search_term_paymodule=&search_term_projects=&date_range_start=&date_range_end=&cluster=any&oldsearch=0&limit=10\" target=\"_blank\">${payments[j].payment_processor_id}</a>`
              }
              res.locals.payments.push(payments[j]);
            }
          });
        }
      });
    res.locals.key_url = `https://metager.de/meta/settings/load-settings?key=${key.get_key()}`;
    next();
  });
});

router.get("/key/:key", (req, res) => {
  res.render("admin/key/overview");
});
router.post(
  "/key/:key",
  oneOf([
    body("amount").isInt({ gt: 0 }),
    body("price").isCurrency({ allow_negatives: false, allow_decimal: true }),
  ]),
  (req, res) => {
    let queryData = matchedData(req, { location: ["body"] });
    let amount = queryData.amount;
    if (!amount) {
      // If amount is not given but price is
      // Calculate amount from price
      let price = queryData.price;
      if (price) {
        amount = Math.ceil(price / config.get("price.per_token"));
      }
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.locals.errors = errors.errors;
      res.render("admin/key/overview");
      return;
    }
    return PaymentReference.CREATE_NEW_REQUEST(
      amount,
      res.locals.key.get_key(),
      undefined,
      true
    )
      .then((payment_reference) => payment_reference.chargeKey())
      .then(() => {
        res.redirect(
          `${res.baseDir
          }/admin/key/${res.locals.key.get_key()}?charge_success=true`
        );
      });
  }
);

router.post(
  "/key/:key/remove-charge",
  body("payment_reference").notEmpty().isInt().toInt(),
  (req, res) => {
    let queryData = matchedData(req, { location: ["body"] });
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.redirect(
        `${res.baseDir
        }/admin/key/${res.locals.key.get_key()}?charge_success=true`
      );
    }
    let payment_reference_id = queryData.payment_reference;
    /** @type {Key} */
    let key = res.locals.key;
    let payment_reference_charge = key.get_charge(payment_reference_id);
    return Key.GET_KEY(key.get_key(), true)
      .then((writable_key) => {
        writable_key.discharge_key(
          payment_reference_charge,
          payment_reference_id
        );
        return writable_key.save();
      })
      .then(() => {
        res.redirect(
          `${res.baseDir
          }/admin/key/${res.locals.key.get_key()}?charge_success=true`
        );
      });
  }
);

module.exports = router;
