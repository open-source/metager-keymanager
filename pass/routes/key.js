var express = require("express");
var router = express.Router();
var multer = require("multer");
const { param, validationResult } = require("express-validator");
const config = require("config");

var orderRouter = require("./orders/orders");
var checkout_router_paypal = require("./checkout/paypal");
var checkout_router_manual = require("./checkout/manual");
var checkout_router_micropayment = require("./checkout/micropayment");
var checkout_router_cash = require("./checkout/cash");

var Key = require("../app/Key");

router.get("/create", function (req, res, next) {
  if (req.cookies.key) {
    return res.redirect(`${res.locals.baseDir}/key/enter`);
  }
  Key.GET_NEW_KEY().then((key) => {
    res.locals.key = key;

    let setting_url = res.baseDir.replace(/\/keys.*/, "");
    setting_url += "/meta/settings/load-settings?";

    let params = {
      key: key.get_key(),
    };

    for (let cookie in req.cookies) {
      if (
        cookie.match(
          /^(dark_mode$|new_tab$|zitate$|web_|bilder_|produkte_|nachrichten_|science_)/
        )
      ) {
        params[cookie] = req.cookies[cookie];
      }
    }
    setting_url += new URLSearchParams(params).toString();
    res.locals.setting_url = setting_url;

    res.render("login/create");
  });
});

router.post("/create", (req, res) => {
  let key = req.body.key;
  Key.GET_KEY(key, false).then((key) => {
    res.redirect(
      `${res.locals.baseDir}/key/` +
      encodeURIComponent(key.get_key()) +
      "#charge"
    );
  });
});

router.get("/remove", (req, res) => {
  if (req.cookies.key) {
    res.clearCookie("key");
  }

  // Check if a redirection URL is supplied
  let url = req.query.url;
  if (!url && typeof req.headers.referer !== "undefined") {
    url = req.headers.referer;
  }
  if (url) {
    let parsed_url = new URL(url);
    if (
      parsed_url.hostname !== req.hostname ||
      parsed_url.pathname.match(/\/key\/.*/)
    ) {
      url = `${res.baseDir}/key/enter`;
    }
  } else {
    url = `${res.baseDir}/key/enter`;
  }

  res.redirect(url);
});

router.get("/enter", function (req, res, next) {
  let key = null;
  if (req.query.key) {
    key = req.query.key;
  } else if (req.headers.key) {
    key = req.headers.key;
  } else if (req.cookies.key) {
    key = req.cookies.key;
  }
  if (key != null) {
    Key.GET_KEY(key, false).then((key) => {
      res.redirect(
        `${res.locals.baseDir}/key/` + encodeURIComponent(key.get_key())
      );
    });
  } else {
    // If the user is using a URL Parameter on MetaGer to use his key there might be a key in the referer
    if (typeof req.headers.referer === "undefined") {
      res.render("login/key");
    }
    let matches = req
      .header("referer")
      .match(
        /\?.*key=([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/
      );
    if (matches) {
      let key_from_referer = matches[1];
      Key.GET_KEY(key_from_referer, false).then((key) => {
        res.redirect(
          `${res.locals.baseDir}/key/` + encodeURIComponent(key.get_key())
        );
      });
    } else {
      res.render("login/key");
    }
  }
});

const upload_storage = multer.memoryStorage();
const upload = multer({
  storage: upload_storage,
  limits: {
    fileSize: 5 * 1024 * 1024,
    files: 1,
    fields: 1,
  },
});
router.post("/enter", upload.single("file"), async (req, res, next) => {
  /** @type {Key} */
  let key = null;
  let error = null;
  if (typeof req.body.key === "string" && req.body.key.length > 0) {
    let input = req.body.key.trim();
    // Login via Login code
    if (input.match(/^\d{6}$/)) {
      let prefix = "logincode:";
      let code = input;
      let key_from_code = await __redis_client.getdel(prefix + code);
      if (key_from_code != null) {
        await __redis_client.del(prefix + key_from_code);
        key = key_from_code;
      } else {
        error = "invalid_login_code";
      }
    } else if (input.match(/^Z\d+$/)) {
      error = "invalid_key_payment_id";
    } else {
      if (input.match(/^[0-9A-F]{32}$/i)) {
        input = [
          input.slice(0, 8),
          input.slice(8, 12),
          input.slice(12, 16),
          input.slice(16, 20),
          input.slice(20, 32),
        ].join("-");
      }
      if (Key.IS_VALID_UUID(input)) {
        input = input.toLowerCase();
      }
      if (Key.IS_VALID_UUID(input) || input.match(/^[0-9a-zA-Z]{6}$/)) {
        key = await Key.GET_KEY(input).then((key) => key.get_key());
      } else {
        error = "invalid_key";
      }
    }
  }

  if (error != null) {
    try {
      let redirect_url = new URL(req.body.redirect_error);
      redirect_url.searchParams.set("key_error", error);
      redirect_url.searchParams.set("invalid_key", req.body.key.trim());
      if (req.host == redirect_url.hostname) {
        res.redirect(redirect_url.toString());
        return;
      }
    } catch (error) { }
    if (error == "invalid_login_code") {
      res.render("login/key", {
        errors: "The login code is invalid. Please check your input.",
      });
      return;
    } else if (error = "invalid_key_payment_id") {
      res.render("login/key", {
        errors: "You've entered a payment id which is not a valid key. Your key has 36 characters.",
      });
      return;
    } else {
      res.render("login/key", {
        errors: "The entered key is invalid. Please check your input.",
      });
      return;
    }
  }

  if (key !== null) {
    try {
      let redirect_url = new URL(req.body.redirect_success);
      redirect_url.searchParams.set("key", key);
      if (req.host == redirect_url.hostname) {
        res.redirect(redirect_url.toString());
        return;
      }
    } catch (error) { }
    res.redirect(`${res.locals.baseDir}/key/` + key);
  } else if (typeof req.file === "undefined") {
    res.render("login/key", { errors: "File not provided or invalid" });
  } else {
    const jimp = require("jimp");

    jimp.read(req.file.buffer, (err, image) => {
      if (err) {
        res.render("login/key", { errors: ["Error reading image data"] });
        return;
      }
      const QrCode = require("qrcode-reader");
      let qr = new QrCode();
      qr.callback = (err, value) => {
        if (err) {
          res.render("login/key", { errors: ["Error decoding QR"] });
          return;
        }
        let url;
        try {
          url = new URL(value.result);
        } catch (err) {
          res.render("login/key", { errors: ["Error parsing URL"] });
          return;
        }
        let key = url.searchParams.get("key");
        if (key !== null) {
          res.redirect(`${res.locals.baseDir}/key/` + encodeURIComponent(key));
        } else {
          res.render("login/key", { errors: ["Error parsing URL"] });
        }
      };
      qr.decode(image.bitmap);
    });
  }
});

router.use("/:key", param("key").isUUID(4), async (req, res, next) => {
  // Input Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  let metager_url = "https://metager.de";
  if (!req.lng.match(/^de/)) {
    metager_url = "https://metager.org";
  }
  metager_url += "/meta/settings/load-settings?";

  let params = {
    key: req.params.key,
  };

  for (let cookie in req.cookies) {
    if (
      cookie.match(
        /^(dark_mode$|new_tab$|zitate$|web_|bilder_|produkte_|nachrichten_|science_)/
      )
    ) {
      params[cookie] = req.cookies[cookie];
    }
  }
  for (let header in req.headers) {
    if (
      header.match(
        /^(dark_mode$|new_tab$|zitate$|web_|bilder_|produkte_|nachrichten_|science_)/
      )
    ) {
      params[header] = req.headers[header];
    }
  }

  metager_url += new URLSearchParams(params).toString();

  Key.GET_KEY(req.params.key, false).then((key) => {
    req.data = {
      price: config.get("price"),
      key: {
        key: key,
        settings_url: metager_url,
        qr: `${res.locals.baseDir}/key/${req.params.key}/qr.png`,
      },
      cookies: req.cookies,
      page: "fill",
      links: {
        fill_url: `${res.locals.baseDir}/key/${req.params.key}`,
        orders_url: `${res.locals.baseDir}/key/${req.params.key}/orders`,
      },
      js: [`${res.locals.baseDir}/js/key.js`],
      css: [`${res.locals.baseDir}/styles/key/key.css`],
    };
    next("route");
  });
});

// Basic account page
router.get("/:key", async (req, res) => {
  if (req.data.admin) {
    res.redirect(`${res.locals.baseDir}/logout`);
    return;
  } else if (
    (!req.cookies.key || req.cookies.key !== req.data.key.key.get_key()) &&
    (!req.headers.key || req.headers.key !== req.data.key.key.get_key())
  ) {
    res.cookie("key", req.data.key.key.get_key(), {
      sameSite: "lax",
      maxAge: 5 * 365 * 24 * 60 * 60 * 1000, // Store for 5 years
      secure: true,
    });
  }
  res.render("key", req.data);
});

router.get("/:key/logincode", (req, res) => {
  req.data.key.key.get_logincode().then((code) => {
    res.set("Cache-Control", "no-store");
    res.json({
      key: req.data.key.key.get_key(),
      code: code,
    });
  });
});

router.get("/:key/qr.png", (req, res) => {
  let metager_url = req.data.key.settings_url;
  let QRCode = require("qrcode");
  res.header({
    "Content-Type": "image/png",
    "Content-Disposition": `inline; filename=metager_key.png`,
  });
  QRCode.toFileStream(res, metager_url, {
    errorCorrectionLevel: "L",
    scale: 16,
  });
});

router.use("/:key/orders", orderRouter);

/**
 * Validate Amount field for checkout process
 */
router.use(
  "/:key/checkout/:amount?",
  param("amount")
    .optional({ checkFalsy: true })
    .isInt()
    .toInt()
    .isIn(config.get("price.purchasable")),
  (req, res, next) => {
    // Input Validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let amount = req.params.amount;
    if (typeof amount !== "undefined" && parseInt(req.params.amount) === 0) {
      amount = undefined;
    } else {
      amount = parseInt(amount);
    }
    req.data.checkout = {
      amount: amount,
    };

    // Add a URL to change the checkout amount
    req.data.change_url = {
      amount:
        `${res.locals.baseDir}/key/` +
        encodeURIComponent(req.data.key.key.get_key()) +
        "#charge",
    };

    next("route");
  }
);

router.get("/:key/checkout/:amount", (req, res) => {
  if (req.query.error) {
    req.data.checkout.error = req.query.error;
  }
  req.data.js.push(`${res.locals.baseDir}/js/funding_sources.js`);
  res.render("key", req.data);
});

// Funding source is selected define some URLs:
router.use("/:key/checkout/:amount?/:payment_source", (req, res, next) => {
  req.data.change_url.funding_source =
    `${res.locals.baseDir}/key/` +
    encodeURIComponent(req.data.key.key.get_key()) +
    "/checkout/" +
    encodeURIComponent(req.data.checkout.amount) +
    "#payment";
  next();
});

router.use("/:key/checkout/:amount?/paypal", checkout_router_paypal);
router.use("/:key/checkout/:amount?/manual", checkout_router_manual);
router.use(
  "/:key/checkout/:amount?/micropayment",
  checkout_router_micropayment
);
router.use("/:key/checkout/:amount?/cash", checkout_router_cash);

module.exports = router;
