var express = require("express");
var router = express.Router();

const { body, validationResult, oneOf } = require("express-validator");

const config = require("config");
const Key = require("../app/Key");
const Crypto = require("../app/Crypto");
const dayjs = require("dayjs");
const NodeRSA = require("node-rsa");
const PaymentReference = require("../app/PaymentReference");

router.post("/key/create",
  authorizedOnly,
  (req, res) => {
    let amount = req.body.amount;
    if (!amount) {
      // If amount is not given but price is
      // Calculate amount from price
      let price = req.body.price;
      if (price) {
        amount = Math.ceil(price / config.get("price.per_token"));
      } else {
        res.status(403).json({
          code: 403,
          error: "Either :amount or :price must be supplied.",
        });
        return;
      }
    }

    return Key.GET_NEW_KEY()
      .then((key) => {
        return PaymentReference.CREATE_NEW_REQUEST(
          amount,
          key.get_key(),
          dayjs().add("10", "years")
        ).then((payment_reference) => {
          return payment_reference.chargeKey().then(() => {
            return res.status(201).json({
              key: key.get_key(),
              payment_reference: payment_reference.public_id,
              charged: payment_reference.amount,
            });
          });
        });
      })
      .catch((reason) => {
        res.status(423).json({
          code: 423,
          error: reason,
          charged: 0,
        });
      });
  });

router.get("/key/:key", (req, res) => {
  Key.GET_KEY(req.params.key, false).then((key) => {
    let key_data = {
      key: key.get_key(),
      charge: key.get_charge(),
      expiration: key.get_expiration_date().format("YYYY-MM-DD HH:mm:ss"),
      charge_orders: key.get_charge_orders(),
    };
    if (!isAuthorized(req)) {
      delete key_data.charge_orders;
      setTimeout(() => res.json(key_data), 250);
    } else {
      res.json(key_data);
    }
  });
});

router.post("/key/:key/discharge",
  authorizedOnly,
  (req, res) => {
    let amount = req.body.amount;
    if (!amount) {
      // If amount is not given but price is
      // Calculate amount from price
      let price = req.body.price;
      if (price) {
        amount = Math.ceil(price / config.get("price.per_token"));
      } else {
        res.status(403).json({
          code: 403,
          error: "Either :amount or :price must be supplied.",
        });
        return;
      }
    }
    let discharged_amount = 0;
    let loaded_key = null;
    Key.GET_KEY(req.params.key, true)
      .then((key) => {
        discharged_amount = key.discharge_key(amount);
        loaded_key = key;
        return key.save();
      })
      .then(() => {
        res.status(201).json({
          key: loaded_key.get_key(),
          charge: loaded_key.get_charge(),
          discharged: discharged_amount,
        });
      });
  });

router.post("/key/:key/charge",
  authorizedOnly,
  (req, res) => {
    let amount = req.body.amount;
    if (!amount) {
      // If amount is not given but price is
      // Calculate amount from price
      let price = req.body.price;
      if (price) {
        amount = Math.ceil(price / config.get("price.per_token"));
      } else {
        res.status(403).json({
          code: 403,
          error: "Either :amount or :price must be supplied.",
        });
        return;
      }
    }
    return Key.GET_KEY(req.params.key)
      .then((key) => {
        return PaymentReference.CREATE_NEW_REQUEST(
          amount,
          key.get_key(),
          dayjs().add("10", "years")
        ).then((payment_reference) => {
          return payment_reference.chargeKey().then(() => {
            return res.status(201).json({
              key: key.get_key(),
              payment_reference: payment_reference.public_id,
              charged: payment_reference.amount,
            });
          });
        });
      })
      .catch((reason) => {
        res.status(423).json({
          code: 423,
          error: reason,
          charged: 0,
        });
      });
  });

/**
 * @deprecated This route is phased out and only used by not updated
 */
router.get("/token/pubkey", async (req, res) => {
  let crypto = new Crypto();
  let now = dayjs();
  /**
   * @type {NodeRSA}
   */
  let private_key = await crypto.get_private_key(now);

  let public_key_components = private_key.exportKey("public");
  res.json({
    date: now.format(config.get("crypto.private_key.date_format")),
    standard: "pcks8",
    format: "pem",
    pubkey_pem: public_key_components,
  });
});

router.get("/token/v2/pubkey", async (req, res) => {
  let token_crypto = new Crypto();
  let decitoken_crypto = new Crypto("decitoken");
  let now = dayjs();
  /**
   * @type {NodeRSA}
   */
  let token_private_key = await token_crypto.get_private_key(now);
  let decitoken_private_key = await decitoken_crypto.get_private_key(now);

  res.json({
    tokens: {
      date: now.format(token_crypto.getDateFormat()),
      standard: "pcks8",
      format: "pem",
      pubkey_pem: token_private_key.exportKey("public"),
    },
    decitokens: {
      date: now.format(decitoken_crypto.getDateFormat()),
      standard: "pcks8",
      format: "pem",
      pubkey_pem: decitoken_private_key.exportKey("public"),
    }
  });
});

/**
 * @deprecated This route is phased out and only used by not updated clients
 */
router.post(
  "/token/sign",
  body("key").notEmpty(),
  body("date")
    .notEmpty()
    .custom((value) => {
      // Make sure the date is either for the last month or this month
      let date = dayjs(value, config.get("crypto.private_key.date_format"));
      let min = dayjs()
        .millisecond(0)
        .second(0)
        .minute(0)
        .hour(0)
        .date(1)
        .subtract(1, "month");
      let max = min.add(2, "month");
      if (!date.isValid() || date.isBefore(min) || !date.isBefore(max)) {
        return Promise.reject("Submitted Date format is invalid");
      }
      return true;
    }),
  body("blinded_tokens")
    .notEmpty()
    .withMessage("Blinded Tokens need to be defined")
    .isArray({ min: 1, max: 10 })
    .withMessage("You can supply between 1 and 10 tokens to sign")
    .custom((value) => {
      let checked_tokens = {};
      for (let i = 0; i < value.length; i++) {
        if (!(typeof value[i] == "string") || value[i].legnth > 1024) {
          return Promise.reject(
            "The supplied tokens can't be more than 1024 characters long"
          );
        }
        if (value[i] in checked_tokens) {
          return Promise.reject("The supplied tokens need to be unique");
        } else {
          checked_tokens[value[i]] = true;
        }
      }
      return true;
    }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json(errors);
      return;
    }

    let date = dayjs(
      req.body.date,
      config.get("crypto.private_key.date_format")
    );
    let blinded_tokens = req.body.blinded_tokens;
    let signed_tokens = {};

    let key = await Key.GET_KEY(req.body.key, true);
    if (key.get_charge() < blinded_tokens.length) {
      await key.save();
      res.status(422).json({
        message: "Invalid Key",
      });
      return;
    } else {
      key.discharge_key(blinded_tokens.length);
      await key.save();
    }

    let crypto = new Crypto();
    await crypto
      .get_private_key(date)
      .then(async (private_key) => {
        for (let i = 0; i < blinded_tokens.length; i++) {
          let blinded_token = blinded_tokens[i];
          let signature = await crypto.sign(blinded_token, private_key);
          signed_tokens[blinded_token] = signature.toString();
        }
      })
      .catch((reason) => {
        console.error(reason);
        res.status(500).json({
          status: 500,
          message: "Couldn't load private key",
        });
        return;
      });

    res.status(201).json({
      key: key.get_key,
      discharged: blinded_tokens.length,
      charge: key.get_charge(),
      date: date.format(config.get("crypto.private_key.date_format")),
      signed_tokens: signed_tokens,
    });
  }
);

router.post(
  "/token/v2/sign",
  body("key").notEmpty(),
  oneOf([
    body("blinded_tokens").exists(),
    body("blinded_decitokens").exists()
  ]),
  body("blinded_tokens").custom((value) => validateBlindedTokenStructure(value, false)),
  body("blinded_decitokens").custom((value) => validateBlindedTokenStructure(value, true)),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json(errors);
      return;
    }


    let blinded_tokens = req.body.blinded_tokens;
    let blinded_decitokens = req.body.blinded_decitokens;
    let signed_tokens = {};
    let signed_decitokens = {};

    let cost = blinded_tokens.tokens.length + (blinded_decitokens.tokens.length / 10);

    let key = await Key.GET_KEY(req.body.key, true);
    if (key.get_charge() < cost) {
      await key.save();
      res.status(422).json({
        message: "Invalid Key",
        charge: key.get_charge()
      });
      return;
    } else {
      key.discharge_key(cost);
      await key.save();
    }

    let token_crypto = new Crypto("token");
    let date = dayjs(
      blinded_tokens.date,
      token_crypto.getDateFormat()
    );
    await token_crypto
      .get_private_key(date)
      .then(async (private_key) => {
        for (let i = 0; i < blinded_tokens.tokens.length; i++) {
          let blinded_token = blinded_tokens.tokens[i];
          let signature = await token_crypto.sign(blinded_token, private_key);
          signed_tokens[blinded_token] = signature.toString();
        }
      })
      .catch((reason) => {
        console.error(reason);
        res.status(500).json({
          status: 500,
          message: "Couldn't load private key",
        });
        return;
      });

    let decitoken_crypto = new Crypto("decitoken");
    date = dayjs(
      blinded_decitokens.date,
      decitoken_crypto.getDateFormat()
    );
    await decitoken_crypto
      .get_private_key(date)
      .then(async (private_key) => {
        for (let i = 0; i < blinded_decitokens.tokens.length; i++) {
          let blinded_token = blinded_decitokens.tokens[i];
          let signature = await decitoken_crypto.sign(blinded_token, private_key);
          signed_decitokens[blinded_token] = signature.toString();
        }
      })
      .catch((reason) => {
        console.error(reason);
        res.status(500).json({
          status: 500,
          message: "Couldn't load private key",
        });
        return;
      });

    res.status(201).json({
      key: key.get_key,
      discharged: cost,
      charge: key.get_charge(),
      signed_tokens: {
        date: blinded_tokens.date,
        tokens: signed_tokens,
      },
      signed_decitokens: {
        date: blinded_decitokens.date,
        tokens: signed_decitokens
      }
    });
  }
);

router.post(
  "/token/check",
  authorizedOnly,
  body("tokens")
    .optional({ values: 'null' }).default([])
    .custom((value) => validateTokenStructure(value, false))
    .bail()
    .custom(async (value) => validateTokenSignature(value, false, false)),
  body("decitokens")
    .optional({ values: 'null' }).default([])
    .custom((value) => validateTokenStructure(value, true))
    .bail()
    .custom(async (value) => validateTokenSignature(value, true, false)),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json(errors);
      return;
    }
    res.json({
      status: "OK",
    });
  }
);

router.post(
  "/token/use",
  authorizedOnly,
  body("tokens")
    .optional({ values: 'null' }).default([])
    .custom((value) => validateTokenStructure(value, false))
    .bail()
    .custom(async (value) => validateTokenSignature(value, false, true)),
  body("decitokens")
    .optional({ values: 'null' }).default([])
    .custom((value) => validateTokenStructure(value, true))
    .bail()
    .custom(async (value) => validateTokenSignature(value, true, true)),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json(errors);
      return;
    }

    res.status(201).json({
      status: "OK",
    });
  }
);

router.use((req, res) => {
  res.status(404).json({
    code: 404,
    error: `API with route ${res.locals.baseDir}${req.originalUrl} not found`,
  });
});

function authorizedOnly(req, res, next) {
  if (!isAuthorized(req)) {
    res.status(401).json({
      code: 401,
      error: "You are not authorized for API usage",
    });
  } else {
    next();
  }
}

function isAuthorized(req) {
  let auth_token = req.get("Authorization");
  let authorized = false;
  if (auth_token) {
    auth_token = auth_token.replace(/^Bearer /, "");
    let required_token = config.get("app.api_token");
    if (required_token === auth_token) {
      authorized = true;
    }
  }
  return authorized;
}

async function validateTokenStructure(tokens = [], decitokens = false) {
  // Validate Tokens are each unique and in expected format

  let error = false;
  let crypto = decitokens ? new Crypto("decitoken") : new Crypto("token");
  let checked_tokens = {};

  for (let i = 0; i < tokens.length; i++) {
    let token = tokens[i];

    if (i > 99) {
      error = true;
      tokens[i]["status"] = "field_exceeded_max_number";
      continue;
    }

    if (!("token" in token || typeof token.token != "string")) {
      error = true;
      tokens[i]["status"] = "field_missing_token";
      continue;
    }
    if (!("signature" in token || typeof token.signature != "string")) {
      error = true;
      tokens[i]["status"] = "field_missing_signature";
      continue;
    }
    if (!("date" in token || typeof token.date != "string")) {
      error = true;
      tokens[i]["status"] = "field_missing_date";
      continue;
    }

    if (
      !token.token.match(
        /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}$/
      )
    ) {
      error = true;
      tokens[i]["status"] = "format_invalid_token";
      continue;
    }

    // Validate supplied expiration date
    let date = dayjs(token.date, crypto.getDateFormat());
    let min = dayjs()
      .millisecond(0)
      .second(0)
      .minute(0)
      .hour(0)
      .date(1)
      .subtract(1, "month");
    let max = min.add(2, "month");
    if (
      !token.date.match(/^\d{4}-(0[1-9]|1[0-2])$/) ||
      !date.isValid() ||
      date.isBefore(min) ||
      !date.isBefore(max)
    ) {
      error = true;
      tokens[i]["status"] = "format_invalid_date";
      continue;
    }

    if (!token.signature.match(/^\d+$/) || token.signature.length > 1024) {
      error = true;
      tokens[i]["status"] = "format_invalid_signature";
      continue;
    }

    if (token.token in checked_tokens) {
      error = true;
      tokens[i]["status"] = "token_not_unique";
      continue;
    } else {
      checked_tokens[token.token] = true;
      tokens[i]["status"] = "ok";
    }
  }

  if (error) {
    return Promise.reject("Token structure is invalid");
  }
  return true;

}

async function validateBlindedTokenStructure(value, decitokens = false) {
  // Make sure the date is either for the last month or this month
  let crypto = new Crypto(decitokens ? "decitoken" : "token");
  if (!value.hasOwnProperty("date") || typeof value.date != "string") {
    return Promise.reject("A Date needs to be specified for blinded tokens");
  }

  let date = dayjs(value.date, crypto.getDateFormat());
  let min = dayjs()
    .millisecond(0)
    .second(0)
    .minute(0)
    .hour(0)
    .date(1)
    .subtract(1, "month");
  let max = min.add(2, "month");
  if (!date.isValid() || date.isBefore(min) || !date.isBefore(max)) {
    return Promise.reject("Submitted Date format is invalid");
  }

  if (!value.hasOwnProperty("tokens") || !Array.isArray(value.tokens)) {
    return Promise.reject("An array of tokens need to be supplied");
  }
  if (value.tokens.length > 100) {
    return Promise.reject("You cannot supply more than 100 tokens");
  }
  let checked_tokens = {};
  for (let i = 0; i < value.tokens.length; i++) {
    if (typeof value.tokens[i] != "string" || value.tokens[i].length > 1024) {
      return Promise.reject({
        msg: "The supplied tokens can't be more than 1024 characters long",
        tokens: checked_tokens
      });
    }
    if (value[i] in checked_tokens) {
      return Promise.reject({ msg: "The supplied tokens need to be unique", checked_tokens: checked_tokens });
    } else {
      checked_tokens[value.tokens[i]] = true;
    }
  }
  return true;
}

async function validateTokenSignature(value, decitokens = false, mark_used = false) {
  // Now that we checked Format of tokens: Check the signature
  let crypto = new Crypto(decitokens ? "decitoken" : "token");
  let error = false;

  let used_tokens = {};

  for (let i = 0; i < value.length; i++) {
    let token = value[i];

    // Check if token was already used
    let tokensource = decitokens ? "decitokens" : "tokens";
    let redis_hash_key = "tokens:used:" + tokensource + ":" + token.date;
    if (await __redis_client.hexists(redis_hash_key, token.token)) {
      error = true;
      value[i]["status"] = "token_already_used";
      continue;
    }

    let verification_result = await crypto.validateToken(
      token.token,
      token.signature,
      token.date
    );
    if (!verification_result) {
      value[i]["status"] = "invalid_signature";
      error = true;
    } else {
      value[i]["status"] = "ok";
      // Tokens can be used. Store the used tokens
      if (mark_used) {
        let redis_hash_expiration = dayjs()
          .add(2, "month")
          .date(1)
          .hour(0)
          .minute(0)
          .second(0)
          .millisecond(0);
        let usage_success = await __redis_client
          .pipeline()
          .hsetnx(
            redis_hash_key,
            token.token,
            dayjs().format("YYYY-MM-DD HH:mm:ss")
          )
          .expireat(redis_hash_key, redis_hash_expiration.unix())
          .exec();
        if (usage_success === 0) {
          error = true;
          value[i]["status"] = "token_already_used";
        } else {
          // Store used tokens in memory so we can rollback in case of later error
          if (!(redis_hash_key in used_tokens)) {
            used_tokens[redis_hash_key] = [];
          }
          used_tokens[redis_hash_key].push(token.token);
        }
      }
    }
  }

  if (error) {
    if (mark_used) {
      // Rollback any used tokens
      for (let redis_hash_key in used_tokens) {
        await __redis_client.hdel(redis_hash_key, used_tokens[redis_hash_key]);
      }
    }
    return Promise.reject("Invalid Signatures");
  } else {
  }
  return true;
}

module.exports = router;
